<?php


use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;




Route::domain('test1')->group(function () {
    Route::get('create_landing/{create_landing}', [LandingController::class, 'show'])->name('test1');
});
Route::domain('test2')->group(function () {
    Route::get('create_landing/{create_landing}', [LandingController::class, 'show'])->name('test2');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('/users', UserController::class);
    Route::resource('/create_landing', LandingController::class)->names('landing');
    Route::get('/test', [TestController::class, 'index']);


});

Auth::routes();

Route::resource('/create-landing', LandingController::class)->names('lending');

