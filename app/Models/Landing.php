<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'shablon_id',
        'title',
        'title_2',
        'image',
        'content',
        'domain',
    ];

    public function users()
    {
        return $this->hasOne(User::class);
    }
    public function shablon()
    {
        return $this->belongsTo(Shablon::class,'shablon_id','id');
    }
}
