<?php

namespace App\Http\Controllers;

use App\Http\Requests\LandingRequest;
use App\Models\Article;
use App\Models\Landing;
use App\Models\Shablon;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $landings = Landing::all();
        return view('admin.landing.index', compact('landings'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shablones = Shablon::all();
        return view('admin.landing.create', compact('shablones'));



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LandingRequest $request)
    {


        $params = $request->all();

        unset($params['image']);
        if ($request->has('image')) {
            $params['image'] = $request->file('image')->store('products');
        }

       Landing::create($params);

        if ($params) {
            return redirect()->route('landing.index')->with('success', 'Landing created successfully');
        } else {
            return back()->withErrors(['msd' => 'Someting was wrong'])->withInput();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Landing $lending
     * @return \Illuminate\Http\Response
     */
    public function show(Landing $create_landing)
    {

        return view('admin.landing.show', compact('create_landing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Landing $landing
     * @return \Illuminate\Http\Response
     */
    public function edit(Landing $landing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Landing $lending
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Landing $landing)
    {
        $path =  public_path().'\images\\';
        $file = $request->file('file');
        $filename = str_random(20) .'.' . $file->getClientOriginalExtension() ?: 'png';
        $img = Image::make($file);
        $img->save($path . $filename);
        echo '/images/'.$filename;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Landing $lending
     * @return \Illuminate\Http\Response
     */
    public function destroy(Landing $lending)
    {

    }
}
