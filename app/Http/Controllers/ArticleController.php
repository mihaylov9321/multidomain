<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Faker\Provider\Image;
use Illuminate\Http\Request;

class ArticleController extends Controller
{


    public function store(Request $request)
    {
        Article::create(['text' => $request->text]);

    }
    public function upload(Request $request)
    {
        $path =  public_path().'\images\\';
        $file = $request->file('file');
        $filename = str_random(20) .'.' . $file->getClientOriginalExtension() ?: 'png';
        $img = Image::make($file);
        $img->save($path . $filename);
        echo '/images/'.$filename;
    }
}
