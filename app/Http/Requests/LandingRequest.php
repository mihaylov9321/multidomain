<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LandingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'title_2'=>'required',
            'image' => 'sometimes|image|mimes:jpg,jpeg,png,svg,gif|max:2048',
            'shablon_id'=>'sometimes',
            'content'=>'sometimes'

        ];
    }
}
