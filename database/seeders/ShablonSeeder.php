<?php

namespace Database\Seeders;

use App\Models\Shablon;
use Illuminate\Database\Seeder;

class ShablonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shablones = [
            [

                'name' => 'shablon1'
            ],
            [

                'name' => 'shablon2'
            ],
            [

                'name' => 'shablon3'
            ]

        ];
        foreach ($shablones as $shablon) {
            Shablon::create($shablon);
        }

    }

}
