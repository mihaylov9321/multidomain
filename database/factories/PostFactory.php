<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Post '.$this->faker->name(),
            'description' => 'Post '.$this->faker->text(),
            'text' => 'Post '.$this->faker->realText(),
            'user_id' =>$this->faker->numberBetween(1,10),
        ];
    }
}
