<?php

return [
  'env_stub' => '.env',
  'storage_dirs' => [
    'app' => [
      'public' => [
      ],
    ],
    'framework' => [
      'cache' => [
      ],
      'testing' => [
      ],
      'sessions' => [
      ],
      'views' => [
      ],
    ],
    'logs' => [
    ],
  ],
  'domains' => [
    'test1' => 'test1',
    'test2' => 'test2',
    'test3' => 'test3',
  ],
 ];