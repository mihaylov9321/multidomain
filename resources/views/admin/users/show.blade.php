@extends('admin.layouts.app')
@section('title')
    {{ $user->name }}
@section('content')

    <div class="container mt-5">
        <div class="row pt-5">
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{ asset('/assets/img/bg5.jpg') }}" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="{{ asset('/assets/img/mike.jpg') }}" alt="...">
                                <h5 class="title">{{ $user->name }}</h5>
                            </a>
                            <p class="description">
                                {{ $user->email }}
                            </p>
                        </div>

                    </div>
                    <hr>
                    <div class="card-footer">
                    <div class="row text-center">
                        <a href="{{ route('users.edit', $user) }}" class="btn mx-auto">Edit</a>
                        <form action="{{ route('users.destroy', $user) }}" method="POST"
                              id="deleteForm-{{ $user->id }}" style="display: none">
                            @csrf
                            @method('DELETE')
                        </form>
                        <button class="btn btn-danger" onclick="
                            if(confirm('Do you want to delete this user?')){
                            event.preventDefault();
                            document.getElementById('deleteForm-{{ $user->id }}').submit();
                            }else{
                            event.preventDefault();
                            }">Delete user
                        </button>
                    </div>
                </div>
            </div>
        </div>
@endsection
