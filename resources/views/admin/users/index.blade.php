@extends('admin.layouts.app')
@section('title', 'users')
@section('content')


    <div class="container mt-5">
        <div class="row pt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title float-left"> Simple Table</h4>
                        <a href="{{ route('users.create') }}" class="btn float-right">Create users</a>
                    </div>
                    @if(session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>Name</th>
                                <th>Email</th>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                <tr onclick="location.href='{{ route('users.show',$user) }}'" style="cursor: pointer;">
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
