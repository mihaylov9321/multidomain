@extends('admin.layouts.app')
@section('title', 'Create users')
@section('content')



    <div class="container mt-5">
        <div class="row pt-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Create users</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('users.store')}}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" name="name" class="form-control" placeholder="Username"
                                               value="{{old('name')}}">
                                    </div>
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email"
                                               value="{{old('email')}}">
                                    </div>
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputPassword">Password</label>
                                        <input type="password" name="password" class="form-control"
                                               placeholder="Password" value="{{old('password')}}">
                                    </div>
                                    @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputText">Role</label>
                                        <input type="role"  name="role" class="form-control" id="exampleInputRole"


                                        </input>

                                    </div>
                                    @error('role_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <button type="submit" class="btn mx-auto">SAVE</button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
