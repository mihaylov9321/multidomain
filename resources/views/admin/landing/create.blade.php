@extends('admin.layouts.app')
@section('title', 'Create users')
@section('content')



    <div class="container">
        <div class="row pt-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Create Lending</h5>
                    </div>

                    <div class="card-body">
                        <form action="{{route('landing.store')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @csrf
                            <div class="row">
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="title"
                                               value="{{old('name')}}">
                                    </div>
                                    @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Title_2</label>
                                        <input type="text" name="title_2" class="form-control" placeholder="title_2"
                                               value="{{old('title_2')}}">
                                    </div>
                                    @error('title_2')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label for="shablon_id">Shablon</label>

                                        <select name="shablon_id" id="shablon_id" class="form-control text-danger"
                                                style="height: 37px">
                                            @foreach($shablones as $shablone)

                                                <option value="{{$shablone->id}}">
                                                    {{$shablone->name}}
                                                </option>
                                            @endforeach

                                        </select>

                                    </div>
                                    @error('shablon')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                </div>

                                <div class="input-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Картинка: </label>
                                    <div class="col-sm-10">
                                        <label class="btn btn-default btn-file">
                                            Загрузить <input type="file" style="display: none;" name="image" id="image">
                                        </label>
                                    </div>
                                </div>
                                <br>
                                @error('image')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 px-1">
                                <div class="form-group">
                                    <label for="text">Content</label><br/>
                                    <textarea name="content" id="content"></textarea><br/>
                                </div>
                                @error('content')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-3 px-1">
                                <div class="form-group">
                                    <label>Domain</label>
                                    <select name="domain" id="domain" class="form-control" style="height: 37px">
                                        <option>
                                            test1
                                        </option>
                                        <option>
                                            test2
                                        </option>
                                    </select>
                                </div>
                                @error('domain')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <button type="submit" class="btn mx-auto">SAVE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
