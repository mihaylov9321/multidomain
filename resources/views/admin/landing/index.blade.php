@extends('admin.layouts.app')
@section('title', 'lending')
@section('content')


    <div class="container mt-5">
        <div class="row pt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header justify-content-between">
                        <h4 class="card-title float-left"> Simple Table</h4>
                        <a href="{{ route('landing.create') }}" class="btn float-right">Create landing</a>
                    </div>
                    @if(session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>Id</th>
                                <th>Title</th>
                                <th>Domain</th>
                                </thead>
                                <tbody>
                                @foreach($landings as $landing)
                                    <tr onclick="location.href='{{ route($landing->domain,$landing) }}'"
                                        style="cursor: pointer;">
                                        <td>{{$landing->id}}</td>
                                        <td>{{$landing->title}}</td>
                                        <td>{{$landing->domain}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
