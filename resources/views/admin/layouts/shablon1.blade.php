
<!DOCTYPE html>
<html lang="ru">
<head>


    <title>!!!</title>

    <link rel="shortcut icon" href="//csite.nicepage.com/favicon.ico" type="image/x-icon">
    <meta charset="utf-8" />
    <meta name="Keywords" content=" ">
    <meta name="Description" content="Знаковые идеи искусства. Профессиональный HTML шаблон. Отзывчивый, полностью настраиваемый с помощью простого редактора Drag-n-Drop. Вы можете использовать его для таких предметов, как идея, культовый, стратегический, брендинг, сеть. Категории: Арт Дизайн.">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://nicepage.com/ru/ht/23071/znakovye-idei-iskusstva-html-shablon">
    <meta property="og:title" content="Знаковые идеи искусства HTML шаблон">
    <meta property="og:image" content="https://images01.nicepage.com/page/23/07/ru/html-shablon-23071.jpg?version=86995bdf-a950-45fe-ae4e-3d305ecf0a7a">
    <meta property="og:description" content="Знаковые идеи искусства. Профессиональный HTML шаблон. Отзывчивый, полностью настраиваемый с помощью простого редактора Drag-n-Drop. Вы можете использовать его для таких предметов, как идея, культовый, стратегический, брендинг, сеть. Категории: Арт Дизайн.">
    <meta property="og:site_name" content="Nicepage.com" />


    <link rel="preload" href="https://images01.nicepage.com/page/23/07/ru/html-shablon-spreview-23071.webp" as="image" type="image/webp" media="(max-width: 450px)">
    <link rel="preload" href="https://images01.nicepage.com/page/23/07/ru/html-shablon-preview-23071.webp" as="image" type="image/webp" media="(min-width: 451px)">




    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no,width=device-width">
    <link href="//fonts.googleapis.com/css?family=Roboto|Open+Sans:200,300,400,700,800,900&amp;subset=latin" rel="preload" as="font" />

{{--   --}}
    <!--
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
    h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
    (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
    })(window,document.documentElement,'async-hide','dataLayer',4000,
    {'GTM-KGP3NM3':true});</script>
    -->        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-88868916-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        var options = {};
        // gtag('config', 'GA_TRACKING_ID', { 'optimize_id': 'OPT_CONTAINER_ID'});
        gtag('config', 'UA-88868916-2', options);
        gtag('config', 'AW-797221335');
    </script>
    <!-- Amplitude Code -->
    <script type="text/javascript">
        (function(e,t){var n=e.amplitude||{_q:[],_iq:{}};var r=t.createElement("script")
        ;r.type="text/javascript"
        ;r.integrity="sha384-d/yhnowERvm+7eCU79T/bYjOiMmq4F11ElWYLmt0ktvYEVgqLDazh4+gW9CKMpYW"
        ;r.crossOrigin="anonymous";r.async=true
        ;r.src="https://cdn.amplitude.com/libs/amplitude-5.2.2-min.gz.js"
        ;r.onload=function(){if(!e.amplitude.runQueuedFunctions){
            console.log("[Amplitude] Error: could not load SDK")}}
        ;var i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)
        ;function s(e,t){e.prototype[t]=function(){
            this._q.push([t].concat(Array.prototype.slice.call(arguments,0)));return this}}
            var o=function(){this._q=[];return this}
            ;var a=["add","append","clearAll","prepend","set","setOnce","unset"]
            ;for(var u=0;u<a.length;u++){s(o,a[u])}n.Identify=o;var c=function(){this._q=[]
                ;return this}
            ;var l=["setProductId","setQuantity","setPrice","setRevenueType","setEventProperties"]
            ;for(var p=0;p<l.length;p++){s(c,l[p])}n.Revenue=c
            ;var d=["init","logEvent","logRevenue","setUserId","setUserProperties","setOptOut","setVersionName","setDomain","setDeviceId","setGlobalUserProperties","identify","clearUserProperties","setGroup","logRevenueV2","regenerateDeviceId","groupIdentify","onInit","logEventWithTimestamp","logEventWithGroups","setSessionId","resetSessionId"]
            ;function v(e){function t(t){e[t]=function(){
                e._q.push([t].concat(Array.prototype.slice.call(arguments,0)))}}
                for(var n=0;n<d.length;n++){t(d[n])}}v(n);n.getInstance=function(e){
                e=(!e||e.length===0?"$default_instance":e).toLowerCase()
                ;if(!n._iq.hasOwnProperty(e)){n._iq[e]={_q:[]};v(n._iq[e])}return n._iq[e]}
            ;e.amplitude=n})(window,document);
        amplitude.getInstance().init("878f4709123a5451aff838c1f870b849");
    </script>
    <script>
        var shareasaleSSCID=shareasaleGetParameterByName("sscid");function shareasaleSetCookie(e,a,r,s,t){if(e&&a){var o,n=s?"; path="+s:"",i=t?"; domain="+t:"",S="";r&&((o=new Date).setTime(o.getTime()+r),S="; expires="+o.toUTCString()),document.cookie=e+"="+a+S+n+i+"; SameSite=None;Secure"}}function shareasaleGetParameterByName(e,a){a||(a=window.location.href),e=e.replace(/[\[\]]/g,"\\$&");var r=new RegExp("[?&]"+e+"(=([^&#]*)|&|#|$)").exec(a);return r?r[2]?decodeURIComponent(r[2].replace(/\+/g," ")):"":null}shareasaleSSCID&&shareasaleSetCookie("shareasaleSSCID",shareasaleSSCID,94670778e4,"/");
    </script>
    <script src="//csite.nicepage.com/BundledScripts/template-detail-libs.js?version=36f6340fefb8f91a0ece3db882db87804d422a81" defer></script>
    <link href="//csite.nicepage.com/BundledScripts/template-detail-libs.css?version=36f6340fefb8f91a0ece3db882db87804d422a81" rel="stylesheet" />


    <link rel="canonical" href="https://nicepage.com/ru/ht/23071/znakovye-idei-iskusstva-html-shablon" />


    <link rel="alternate" hreflang="en" href="https://nicepage.com/ht/23071/iconic-ideas-of-art-html-template">
    <link rel="alternate" hreflang="de" href="https://nicepage.com/de/ht/23071/ikonenhafte-ideen-der-kunst-html-vorlage">
    <link rel="alternate" hreflang="nl" href="https://nicepage.com/nl/ht/23071/iconische-ideeen-over-kunst-html-sjabloon">
    <link rel="alternate" hreflang="fr" href="https://nicepage.com/fr/ht/23071/idees-iconiques-de-lart-modele-html">
    <link rel="alternate" hreflang="it" href="https://nicepage.com/it/ht/23071/idee-iconiche-darte-modello-html">
    <link rel="alternate" hreflang="es" href="https://nicepage.com/es/ht/23071/ideas-iconicas-de-arte-plantilla-html">
    <link rel="alternate" hreflang="pl" href="https://nicepage.com/pl/ht/23071/kultowe-idee-sztuki-szablon-html">
    <link rel="alternate" hreflang="pt" href="https://nicepage.com/pt/ht/23071/ideias-iconicas-de-arte-modelo-html">
    <link rel="alternate" hreflang="tr" href="https://nicepage.com/tr/ht/23071/ikonik-sanat-fikirleri-html-sablonu">
    <link rel="alternate" hreflang="ru" href="https://nicepage.com/ru/ht/23071/znakovye-idei-iskusstva-html-shablon">
    <link rel="alternate" hreflang="hu" href="https://nicepage.com/hu/ht/23071/ikonikus-oetletek-a-muveszetrol-html-sablon">
    <link rel="alternate" hreflang="cs" href="https://nicepage.com/cs/ht/23071/ikonicke-myslenky-umeni-sablona-html">
    <link rel="alternate" hreflang="sv" href="https://nicepage.com/sv/ht/23071/ikoniska-ideer-om-konst-html-mall">



    <style>
        h1 {
            text-transform: capitalize;
            font-size: 32px;
            margin: 10px 0 20px;
        }

        h2 {
            font-size: 28px;
        }

        .download-btn-container {
            padding-bottom: 10px;
        }

        .preview-row .media-image {
            border: none;
            aspect-ratio: attr(width) / attr(height);
        }

        .preview-row {
            position: relative;
            margin-bottom: 40px;
        }

        .preview-row img,
        .preview-row .hover-like-lightbox {
            display: inline-block;
        }

        .preview-row .hover-like-lightbox {
            overflow: visible;
        }

        .hover-like-lightbox:hover:after {
            display: block;
            content: "";
            background: rgba(0, 0, 0, .2) 100% 100% no-repeat;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .mockup-shadow {
            max-width: 100%;
        }

        .mockup-shadow:before {
            bottom: 0;
            box-shadow: 3px 3px 15px rgba(0, 0, 0, .3);
            display: block;
            content: '';
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            z-index: -1;
        }

        .small-icon {
            vertical-align: middle;
            display: inline-block;
        }

        .u-icon {
            vertical-align: middle;
            height: 40px;
            display: inline-block;
        }

        .icon-g {
            fill: #999;
            width: 48px;
            height: 48px;
            position: relative;
            top: 8px;
            margin-right: 12px;
        }

        .page-description-block {
            margin-top: 30px;
            font-size: 18px;
            line-height: 1.75;
            text-align: left;
        }

        .auth-page-description-block {
            line-height: 1.75;
            font-size: 14px;
        }

        .page-subtitle {
            margin: 20px 0;
            font-size: 18px;
            line-height: 1.75;
        }

        .page-description-block h3 {
            margin-bottom: 20px;
        }

        .win-download,
        .mac-download,
        .edit-online-btn {
            color: #fff;
            padding: 10px 30px;
            margin-bottom: 10px;
            font-size: 20px;
            line-height: 1.4;
            display: inline-block;
        }

        .top-btn {
            margin: 0 0 20px 0;
        }

        @media (min-width: 991px) and (max-width: 1200px) {
            .win-download, .mac-download, .edit-online-btn {
                padding: 10px 26px;
                font-size: 16px;
            }
        }

        @media(max-width: 768px) {
            .win-download, .mac-download, .edit-online-btn {
                margin: 10px 0;
                font-size: 16px;
            }
        }


        .mac-download {
            margin-left: 10px;
            background-color: #4399fa;
            border-color: #4399fa;
        }

        .mac-download:hover, .mac-download:focus {
            color: #fff;
            background-color: #2e8dfa;
            border-color: #2e8dfa;
        }

        .win-download {
            background-color: #f15048;
            border-color: #f15048;
        }

        .win-download:hover, .win-download:focus {
            color: #fff;
            background-color: #ef342b;
            border-color: #ef342b;
        }

        .edit-online-btn {
            margin-left: 10px;
            background-color: #5cb85c;
            border-color: #5cb85c;
        }
        .edit-online-btn.top-btn {
            margin: 0 0 20px 0;
            max-width: 335px;
            width: 100%;
        }

        .edit-online-btn:hover, .edit-online-btn:focus {
            color: #fff;
            background-color: #4bad4b;
            border-color: #4bad4b;
        }

        .feature-icon-label {
            vertical-align: -0.17em;
            margin-left: 10px;
        }

        @media (min-width: 992px) and (max-width: 1280px) {
            #register-input {
                width: 150px;
                font-size: 16px;
                padding: 6px;
            }

            #register-btn {
                font-size: 16px;
                border: none;
                padding: 7px 12px;
                font-weight: 700;
            }

            .feature-icon-label {
                font-size: 14px;
                margin-left: 5px;
            }
        }

        .u-svg-link {
            fill: #f15048;
            height: 40px;
            width: 40px;
            color: #f15048;
            position: relative;
        }

        .u-svg-content {
            width: 0;
            height: 0;
        }

        .right-feature-column {
            padding: 0 10px;
        }

        @media (max-width: 992px) {
            .right-feature-column {
                padding: 0 15px;
            }
        }

        @media (max-width: 768px) {
            .right-feature-column {
                padding-top: 10px;
            }
        }

        .details-keywords {
            margin-left: -10px;
        }

        .details-keywords a {
            display: inline-block;
            padding: 2px 10px;
            margin: 0 0 5px 10px;
            font-size: 13px;
            color: #777;
            border: 1px solid #cecece;
            border-radius: 20px;
            text-transform: lowercase;
            transition: .2s;
        }

        .details-keywords a:hover {
            text-decoration: none;
            color: #337ab7;
            border-color: #337ab7;
        }

        .details-keywords i {
            position: absolute;
            opacity: 0;
        }

        .customize-np-block {
            margin-top: 30px;
            margin-bottom: 20px;
            font-size: 14px;
        }

        .modal .container-layout {
            padding: 20px;
        }

        .modal .u-text {
            font-size: 28px;
            margin: 20px auto 0;
        }
    </style>



    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            if (typeof IOlazy === 'function') {
                var lazy = new IOlazy();
            }
        });
    </script>
</head>
<body>

<div class="wrap wrap-fluid">
    <div id="top-navbar" class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand clearfix" href="https://nicepage.com"><img class="pull-left" width="123" height="40" alt="Nicepage.com" src="//csite.nicepage.com/Images/logo-w.png" /></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="logon-block nav navbar-nav nav-list navbar-right u-unstyled">


                </ul>
            </div>
        </div>
    </div>


    <section id="" class="container">






        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <style>
                            .breadcrumb {
                                padding: 0;
                                background-color: transparent;
                            }

                            .breadcrumb > .active {
                                color: #666;
                            }
                        </style>
                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>


                            <h1>{{$create_landing->title}}</h1>



                        <div style="margin-bottom: 20px; font-size: 14px;">
                            {{$create_landing->title_2}}
                        </div>

                        <div style="margin-bottom: 20px;">
                            <div class="pull-left">
                                ID <text>: </text>23071
                                &nbsp; | &nbsp;
                                Категория                            :
                                <a href="/ru/c/art-dizayn-html-shablony">Арт Дизайн</a>                    </div>
                            <div class="pull-right">
                                <a href="/ru/html-templates/preview/znakovye-idei-iskusstva-23071?device=desktop" target="_blank" class="demo-link" rel="nofollow">
                                    <div class="small-icon">
                                        <svg id="preview" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
                                    <path fill="#808080" d="M12.6,11.8c0.4-0.6,0.7-1.3,0.7-2.2c0-2-1.6-3.7-3.7-3.7S6,7.6,6,9.7s1.6,3.7,3.7,3.7c0.8,0,1.5-0.3,2.2-0.7
                                          l3.4,3.4l0.8-0.8C16,15.2,12.6,11.8,12.6,11.8L12.6,11.8z M9.7,12.2c-1.4,0-2.5-1.1-2.5-2.5s1.1-2.5,2.5-2.5s2.5,1.1,2.5,2.5
                                          S11.1,12.2,9.7,12.2L9.7,12.2z" />
                                            <polygon fill="none" points="8,4 8,1 1,1 1,15 11,15 11,15 3.5,15 3.5,5.5 11,5.5 11,4 " />
                                            <polygon fill="#808080" points="11,15 1,15 1,1 8,1 8,4 11,4 11,5.5 12,5.5 12,3.4 8.3,0 0,0 0,16 12,16 12,14 11,14 " />
                                </svg>
                                    </div>
                                    <span>Предварительный просмотр демо</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="preview-row">

                                <td><img src="{{ Storage::url($create_landing->image) }}"></td>


                        </div>
                        <div class=" position-ref full-height">
                            <div class="content">


                                    <div>{!! $create_landing->content !!}</div>
                                    <hr><br>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Больше от Арт Дизайн HTML шаблоны</h3>



                <ul class="thumbnails thumbnails-loading list-unstyled">
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 567px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/103312/stili-illyustratsiy-dlya-iskusstva-html-shablon" style="width:260px; height: 517px;" title="Стили Иллюстраций Для Искусства"><img alt="Стили Иллюстраций Для Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/10/33/ru/html-shablon-103312.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/10/33/ru/html-shablon-full-103312.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/103312/stili-illyustratsiy-dlya-iskusstva-html-shablon" title="Стили Иллюстраций Для Искусства">Стили Иллюстраций Для Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 572px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/167853/napravleniya-dizayn-studii-html-shablon" style="width:260px; height: 522px;" title="Направления Дизайн-Студии"><img alt="Направления Дизайн-Студии" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/16/78/ru/html-shablon-167853.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/16/78/ru/html-shablon-full-167853.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/167853/napravleniya-dizayn-studii-html-shablon" title="Направления Дизайн-Студии">Направления Дизайн-Студии</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 650px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/17164/kollektsiya-proizvedeniy-iskusstva-html-shablon" style="width:260px; height: 600px;" title="Коллекция Произведений Искусства"><img alt="Коллекция Произведений Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/17/16/ru/html-shablon-17164.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/17/16/ru/html-shablon-full-17164.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/17164/kollektsiya-proizvedeniy-iskusstva-html-shablon" title="Коллекция Произведений Искусства">Коллекция Произведений Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 725px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/266717/rezyume-veb-dizaynera-html-shablon" style="width:260px; height: 675px;" title="Резюме Веб-Дизайнера"><img alt="Резюме Веб-Дизайнера" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/26/67/ru/html-shablon-266717.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/26/67/ru/html-shablon-full-266717.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/266717/rezyume-veb-dizaynera-html-shablon" title="Резюме Веб-Дизайнера">Резюме Веб-Дизайнера</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 627px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/111572/veb-dizayn-ot-nashey-studii-html-shablon" style="width:260px; height: 577px;" title="Веб-Дизайн От Нашей Студии"><img alt="Веб-Дизайн От Нашей Студии" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/11/15/ru/html-shablon-111572.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/11/15/ru/html-shablon-full-111572.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/111572/veb-dizayn-ot-nashey-studii-html-shablon" title="Веб-Дизайн От Нашей Студии">Веб-Дизайн От Нашей Студии</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 717px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/427433/mezhdunarodnoe-agentstvo-brendov-i-dizayna-html-shablon" style="width:260px; height: 667px;" title="Международное Агентство Брендов И Дизайна"><img alt="Международное Агентство Брендов И Дизайна" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/42/74/ru/html-shablon-427433.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/42/74/ru/html-shablon-full-427433.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/427433/mezhdunarodnoe-agentstvo-brendov-i-dizayna-html-shablon" title="Международное Агентство Брендов И Дизайна">Международное Агентство Брендов И Дизайна</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 640px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/17311/ivent-agentstvo-html-shablon" style="width:260px; height: 590px;" title="Ивент Агентство"><img alt="Ивент Агентство" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/17/31/ru/html-shablon-17311.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/17/31/ru/html-shablon-full-17311.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/17311/ivent-agentstvo-html-shablon" title="Ивент Агентство">Ивент Агентство</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 844px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/24552/otkryvaya-urovni-iskusstva-html-shablon" style="width:260px; height: 794px;" title="Открывая Уровни Искусства"><img alt="Открывая Уровни Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/24/55/ru/html-shablon-24552.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/24/55/ru/html-shablon-full-24552.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/24552/otkryvaya-urovni-iskusstva-html-shablon" title="Открывая Уровни Искусства">Открывая Уровни Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 788px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/402517/sozdaem-tsifrovye-produkty-i-uslugi-html-shablon" style="width:260px; height: 738px;" title="Создаем Цифровые Продукты И Услуги"><img alt="Создаем Цифровые Продукты И Услуги" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/40/25/ru/html-shablon-402517.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/40/25/ru/html-shablon-full-402517.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/402517/sozdaem-tsifrovye-produkty-i-uslugi-html-shablon" title="Создаем Цифровые Продукты И Услуги">Создаем Цифровые Продукты И Услуги</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 620px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/82540/brending-dlya-biznesa-html-shablon" style="width:260px; height: 570px;" title="Брендинг Для Бизнеса HTML Шаблон"><img alt="Брендинг Для Бизнеса HTML Шаблон" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/82/54/ru/html-shablon-82540.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/82/54/ru/html-shablon-full-82540.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/82540/brending-dlya-biznesa-html-shablon" title="Брендинг Для Бизнеса HTML Шаблон">Брендинг Для Бизнеса HTML Шаблон</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" style="width: 260px; height: 460px;">
                        <div class="download-block white-block" style="background-color: #ffffff; ">
                            <img class="lazyload" data-src="//images01.nicepage.com/thumbs/a122014e1e8cdf24af08f98c/42d0c92512c25ddbb07a0447/template-builder-color_150.gif" alt="Легкий конструктор шаблонов" />
                            <div style="color: #3c3c3c;" class="download-title">Легкий конструктор шаблонов</div>
                            <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow" style="background-color: #ec311e;">Скачать</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 648px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/13011/tvorcheskaya-galereya-html-shablon" style="width:260px; height: 598px;" title="Творческая Галерея"><img alt="Творческая Галерея" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/13/01/ru/html-shablon-13011.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/13/01/ru/html-shablon-full-13011.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/13011/tvorcheskaya-galereya-html-shablon" title="Творческая Галерея">Творческая Галерея</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 549px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/72060/vazhnye-detali-krasoty-html-shablon" style="width:260px; height: 499px;" title="Важные Детали Красоты"><img alt="Важные Детали Красоты" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/72/06/ru/html-shablon-72060.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/72/06/ru/html-shablon-full-72060.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/72060/vazhnye-detali-krasoty-html-shablon" title="Важные Детали Красоты">Важные Детали Красоты</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 630px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/127169/informatsiya-o-tendentsiyakh-krasoty-html-shablon" style="width:260px; height: 580px;" title="Информация О Тенденциях Красоты"><img alt="Информация О Тенденциях Красоты" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/12/71/ru/html-shablon-127169.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/12/71/ru/html-shablon-full-127169.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/127169/informatsiya-o-tendentsiyakh-krasoty-html-shablon" title="Информация О Тенденциях Красоты">Информация О Тенденциях Красоты</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 849px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/30803/sozdanie-originalnykh-resheniy-html-shablon" style="width:260px; height: 799px;" title="Создание Оригинальных Решений"><img alt="Создание Оригинальных Решений" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/30/80/ru/html-shablon-30803.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/30/80/ru/html-shablon-full-30803.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/30803/sozdanie-originalnykh-resheniy-html-shablon" title="Создание Оригинальных Решений">Создание Оригинальных Решений</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 772px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/117349/dizayn-studiya-polnogo-tsikla-html-shablon" style="width:260px; height: 722px;" title="Дизайн-Студия Полного Цикла"><img alt="Дизайн-Студия Полного Цикла" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/11/73/ru/html-shablon-117349.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/11/73/ru/html-shablon-full-117349.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/117349/dizayn-studiya-polnogo-tsikla-html-shablon" title="Дизайн-Студия Полного Цикла">Дизайн-Студия Полного Цикла</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 644px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/14287/muzey-tsifrovogo-iskusstva-html-shablon" style="width:260px; height: 594px;" title="Музей Цифрового Искусства"><img alt="Музей Цифрового Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/14/28/ru/html-shablon-14287.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/14/28/ru/html-shablon-full-14287.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/14287/muzey-tsifrovogo-iskusstva-html-shablon" title="Музей Цифрового Искусства">Музей Цифрового Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 544px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/98781/tvorcheskaya-studiya-innovatsionnogo-iskusstva-html-shablon" style="width:260px; height: 494px;" title="Творческая Студия Инновационного Искусства"><img alt="Творческая Студия Инновационного Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/98/78/ru/html-shablon-98781.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/98/78/ru/html-shablon-full-98781.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/98781/tvorcheskaya-studiya-innovatsionnogo-iskusstva-html-shablon" title="Творческая Студия Инновационного Искусства">Творческая Студия Инновационного Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 610px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/40935/novoe-dlya-vashego-razvitiya-html-shablon" style="width:260px; height: 560px;" title="Новое Для Вашего Развития"><img alt="Новое Для Вашего Развития" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/40/93/ru/html-shablon-40935.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/40/93/ru/html-shablon-full-40935.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/40935/novoe-dlya-vashego-razvitiya-html-shablon" title="Новое Для Вашего Развития">Новое Для Вашего Развития</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 673px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/350887/portfolio-modelera-html-shablon" style="width:260px; height: 623px;" title="Портфолио Модельера"><img alt="Портфолио Модельера" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/35/08/ru/html-shablon-350887.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/35/08/ru/html-shablon-full-350887.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/350887/portfolio-modelera-html-shablon" title="Портфолио Модельера">Портфолио Модельера</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 947px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/565625/studiya-dizayna-i-brendinga-html-shablon" style="width:260px; height: 897px;" title="Студия Дизайна И Брендинга HTML Шаблон"><img alt="Студия Дизайна И Брендинга HTML Шаблон" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/56/56/ru/html-shablon-565625.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/56/56/ru/html-shablon-full-565625.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/565625/studiya-dizayna-i-brendinga-html-shablon" title="Студия Дизайна И Брендинга HTML Шаблон">Студия Дизайна И Брендинга HTML Шаблон</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" style="width: 260px; height: 460px;">
                        <div class="download-block " style="background-color: #363636; ">
                            <img class="lazyload" data-src="//images01.nicepage.com/thumbs/a122014e1e8cdf24af08f98c/8739790df40451e683e5a152/templates-small-animation-dark_150.gif" alt="Самая большая коллекция шаблонов" />
                            <div style="color: #ffffff;" class="download-title">Самая большая коллекция шаблонов</div>
                            <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow" style="background-color: #cd30c0;">Скачать</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 723px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/24349/tvorcheskie-khudozhestvennye-idei-html-shablon" style="width:260px; height: 673px;" title="Творческие Художественные Идеи"><img alt="Творческие Художественные Идеи" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/24/34/ru/html-shablon-24349.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/24/34/ru/html-shablon-full-24349.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/24349/tvorcheskie-khudozhestvennye-idei-html-shablon" title="Творческие Художественные Идеи">Творческие Художественные Идеи</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 670px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/32528/tvorcheskaya-laboratoriya-tsifrovogo-iskusstva-html-shablon" style="width:260px; height: 620px;" title="Творческая Лаборатория Цифрового Искусства"><img alt="Творческая Лаборатория Цифрового Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/32/52/ru/html-shablon-32528.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/32/52/ru/html-shablon-full-32528.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/32528/tvorcheskaya-laboratoriya-tsifrovogo-iskusstva-html-shablon" title="Творческая Лаборатория Цифрового Искусства">Творческая Лаборатория Цифрового Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 634px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/296670/20-let-opyta-raboty-s-brendami-html-shablon" style="width:260px; height: 584px;" title="20 Лет Опыта Работы С Брендами"><img alt="20 Лет Опыта Работы С Брендами" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/29/66/ru/html-shablon-296670.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/29/66/ru/html-shablon-full-296670.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/296670/20-let-opyta-raboty-s-brendami-html-shablon" title="20 Лет Опыта Работы С Брендами">20 Лет Опыта Работы С Брендами</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 649px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/447109/novaya-kreativnaya-studiya-html-shablon" style="width:260px; height: 599px;" title="Новая Креативная Студия"><img alt="Новая Креативная Студия" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/44/71/ru/html-shablon-447109.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/44/71/ru/html-shablon-full-447109.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/447109/novaya-kreativnaya-studiya-html-shablon" title="Новая Креативная Студия">Новая Креативная Студия</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 706px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/27464/vremya-veb-dizayna-html-shablon" style="width:260px; height: 656px;" title="Время Веб-Дизайна"><img alt="Время Веб-Дизайна" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/27/46/ru/html-shablon-27464.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/27/46/ru/html-shablon-full-27464.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/27464/vremya-veb-dizayna-html-shablon" title="Время Веб-Дизайна">Время Веб-Дизайна</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 570px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/28597/roza-html-shablon" style="width:260px; height: 520px;" title="Роза"><img alt="Роза" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/28/59/ru/html-shablon-28597.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/28/59/ru/html-shablon-full-28597.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/28597/roza-html-shablon" title="Роза">Роза</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 1035px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/31575/prostaya-zhizn-html-shablon" style="width:260px; height: 985px;" title="Простая Жизнь"><img alt="Простая Жизнь" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/31/57/ru/html-shablon-31575.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/31/57/ru/html-shablon-full-31575.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/31575/prostaya-zhizn-html-shablon" title="Простая Жизнь">Простая Жизнь</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 600px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/14351/tendentsii-dizayna-html-shablon" style="width:260px; height: 550px;" title="Тенденции Дизайна"><img alt="Тенденции Дизайна" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/14/35/ru/html-shablon-14351.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/14/35/ru/html-shablon-full-14351.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/14351/tendentsii-dizayna-html-shablon" title="Тенденции Дизайна">Тенденции Дизайна</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 638px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/206503/tsvetochnyy-salon-html-shablon" style="width:260px; height: 588px;" title="Цветочный Салон"><img alt="Цветочный Салон" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/20/65/ru/html-shablon-206503.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/20/65/ru/html-shablon-full-206503.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/206503/tsvetochnyy-salon-html-shablon" title="Цветочный Салон">Цветочный Салон</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 580px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/19085/brendingovoe-agentstvo-dlya-startapa-html-shablon" style="width:260px; height: 530px;" title="Брендинговое Агентство Для Стартапа HTML Шаблон"><img alt="Брендинговое Агентство Для Стартапа HTML Шаблон" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/19/08/ru/html-shablon-19085.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/19/08/ru/html-shablon-full-19085.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/19085/brendingovoe-agentstvo-dlya-startapa-html-shablon" title="Брендинговое Агентство Для Стартапа HTML Шаблон">Брендинговое Агентство Для Стартапа HTML Шаблон</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" style="width: 260px; height: 280px;">
                        <div class="download-block white-block" style="background-color: #ffffff; ">
                            <div style="color: #3c3c3c;" class="download-title">Настроить любой шаблон</div>
                            <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow" style="background-color: #ec311e;">Скачать</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 952px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/28654/brendy-budushchego-s-luchshim-iskusstvom-html-shablon" style="width:260px; height: 902px;" title="Бренды Будущего С Лучшим Искусством"><img alt="Бренды Будущего С Лучшим Искусством" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/28/65/ru/html-shablon-28654.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/28/65/ru/html-shablon-full-28654.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/28654/brendy-budushchego-s-luchshim-iskusstvom-html-shablon" title="Бренды Будущего С Лучшим Искусством">Бренды Будущего С Лучшим Искусством</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 765px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/669534/kak-nayti-dizaynera-html-shablon" style="width:260px; height: 715px;" title="Как Найти Дизайнера"><img alt="Как Найти Дизайнера" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/66/95/ru/html-shablon-669534.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/66/95/ru/html-shablon-full-669534.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/669534/kak-nayti-dizaynera-html-shablon" title="Как Найти Дизайнера">Как Найти Дизайнера</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 669px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/514500/o-sotrudnichestve-html-shablon" style="width:260px; height: 619px;" title="О Сотрудничестве"><img alt="О Сотрудничестве" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/51/45/ru/html-shablon-514500.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/51/45/ru/html-shablon-full-514500.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/514500/o-sotrudnichestve-html-shablon" title="О Сотрудничестве">О Сотрудничестве</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 580px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/39402/borba-so-stereotipami-v-iskusstve-html-shablon" style="width:260px; height: 530px;" title="Борьба Со Стереотипами В Искусстве"><img alt="Борьба Со Стереотипами В Искусстве" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/39/40/ru/html-shablon-39402.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/39/40/ru/html-shablon-full-39402.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/39402/borba-so-stereotipami-v-iskusstve-html-shablon" title="Борьба Со Стереотипами В Искусстве">Борьба Со Стереотипами В Искусстве</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 575px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/79786/cherno-belye-tsveta-iskusstva-html-shablon" style="width:260px; height: 525px;" title="Черно-Белые Цвета Искусства"><img alt="Черно-Белые Цвета Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/79/78/ru/html-shablon-79786.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/79/78/ru/html-shablon-full-79786.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/79786/cherno-belye-tsveta-iskusstva-html-shablon" title="Черно-Белые Цвета Искусства">Черно-Белые Цвета Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 520px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/21755/muzykalnye-festivali-html-shablon" style="width:260px; height: 470px;" title="Музыкальные Фестивали"><img alt="Музыкальные Фестивали" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/21/75/ru/html-shablon-21755.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/21/75/ru/html-shablon-full-21755.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/21755/muzykalnye-festivali-html-shablon" title="Музыкальные Фестивали">Музыкальные Фестивали</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 575px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/221809/vse-o-kreativnom-dizayne-html-shablon" style="width:260px; height: 525px;" title="Все О Креативном Дизайне"><img alt="Все О Креативном Дизайне" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/22/18/ru/html-shablon-221809.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/22/18/ru/html-shablon-full-221809.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/221809/vse-o-kreativnom-dizayne-html-shablon" title="Все О Креативном Дизайне">Все О Креативном Дизайне</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 640px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/697431/sovremennyy-progressivnyy-dizayn-html-shablon" style="width:260px; height: 590px;" title="Современный Прогрессивный Дизайн"><img alt="Современный Прогрессивный Дизайн" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/69/74/ru/html-shablon-697431.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/69/74/ru/html-shablon-full-697431.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/697431/sovremennyy-progressivnyy-dizayn-html-shablon" title="Современный Прогрессивный Дизайн">Современный Прогрессивный Дизайн</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 599px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/255724/vmeste-my-sozdaem-krasotu-i-stil-html-shablon" style="width:260px; height: 549px;" title="Вместе Мы Создаем Красоту И Стиль"><img alt="Вместе Мы Создаем Красоту И Стиль" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/25/57/ru/html-shablon-255724.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/25/57/ru/html-shablon-full-255724.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/255724/vmeste-my-sozdaem-krasotu-i-stil-html-shablon" title="Вместе Мы Создаем Красоту И Стиль">Вместе Мы Создаем Красоту И Стиль</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 528px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/104927/idei-v-tvorcheskom-portfolio-html-shablon" style="width:260px; height: 478px;" title="Идеи В Творческом Портфолио HTML Шаблон"><img alt="Идеи В Творческом Портфолио HTML Шаблон" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/10/49/ru/html-shablon-104927.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/10/49/ru/html-shablon-full-104927.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/104927/idei-v-tvorcheskom-portfolio-html-shablon" title="Идеи В Творческом Портфолио HTML Шаблон">Идеи В Творческом Портфолио HTML Шаблон</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" style="width: 260px; height: 320px;">
                        <div class="download-block " style="background-color: #363636; ">
                            <div style="color: #ffffff;" class="download-title">Создайте свой собственный сайт</div>
                            <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow" style="background-color: #cd30c0;">Скачать</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 742px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/554840/originalnye-mobilnye-prilozheniya-html-shablon" style="width:260px; height: 692px;" title="Оригинальные Мобильные Приложения"><img alt="Оригинальные Мобильные Приложения" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/55/48/ru/html-shablon-554840.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/55/48/ru/html-shablon-full-554840.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/554840/originalnye-mobilnye-prilozheniya-html-shablon" title="Оригинальные Мобильные Приложения">Оригинальные Мобильные Приложения</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 692px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/31866/revolyutsiya-dizaynerskogo-iskusstva-html-shablon" style="width:260px; height: 642px;" title="Революция Дизайнерского Искусства"><img alt="Революция Дизайнерского Искусства" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/31/86/ru/html-shablon-31866.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/31/86/ru/html-shablon-full-31866.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/31866/revolyutsiya-dizaynerskogo-iskusstva-html-shablon" title="Революция Дизайнерского Искусства">Революция Дизайнерского Искусства</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 621px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/376824/rezyume-na-dolzhnost-html-shablon" style="width:260px; height: 571px;" title="Резюме На Должность"><img alt="Резюме На Должность" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/37/68/ru/html-shablon-376824.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/37/68/ru/html-shablon-full-376824.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/376824/rezyume-na-dolzhnost-html-shablon" title="Резюме На Должность">Резюме На Должность</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 674px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/414115/sozdaem-krasivye-sayty-html-shablon" style="width:260px; height: 624px;" title="Создаем Красивые Сайты"><img alt="Создаем Красивые Сайты" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/41/41/ru/html-shablon-414115.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/41/41/ru/html-shablon-full-414115.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/414115/sozdaem-krasivye-sayty-html-shablon" title="Создаем Красивые Сайты">Создаем Красивые Сайты</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 548px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/126526/professionalnyy-fotograf-html-shablon" style="width:260px; height: 498px;" title="Профессиональный Фотограф"><img alt="Профессиональный Фотограф" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/12/65/ru/html-shablon-126526.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/12/65/ru/html-shablon-full-126526.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/126526/professionalnyy-fotograf-html-shablon" title="Профессиональный Фотограф">Профессиональный Фотограф</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 505px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/18600/fotograf-html-shablon" style="width:260px; height: 455px;" title="Фотограф"><img alt="Фотограф" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/18/60/ru/html-shablon-18600.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/18/60/ru/html-shablon-full-18600.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/18600/fotograf-html-shablon" title="Фотограф">Фотограф</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 623px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/391290/vdokhnovenie-html-shablon" style="width:260px; height: 573px;" title="Вдохновение"><img alt="Вдохновение" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/39/12/ru/html-shablon-391290.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/39/12/ru/html-shablon-full-391290.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/391290/vdokhnovenie-html-shablon" title="Вдохновение">Вдохновение</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 610px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/23325/zdravstvuyte-my-dizaynerskoe-agentstvo-html-shablon" style="width:260px; height: 560px;" title="Здравствуйте, Мы Дизайнерское Агентство"><img alt="Здравствуйте, Мы Дизайнерское Агентство" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/23/32/ru/html-shablon-23325.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/23/32/ru/html-shablon-full-23325.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/23325/zdravstvuyte-my-dizaynerskoe-agentstvo-html-shablon" title="Здравствуйте, Мы Дизайнерское Агентство">Здравствуйте, Мы Дизайнерское Агентство</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 726px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/31336/iskusstvo-html-shablon" style="width:260px; height: 676px;" title="Искусство"><img alt="Искусство" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/31/33/ru/html-shablon-31336.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/31/33/ru/html-shablon-full-31336.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/31336/iskusstvo-html-shablon" title="Искусство">Искусство</a>
                        </div>
                    </li>
                    <li class="thumbnail-item" data-height="0" style="width: 260px; height: 636px;">
                        <div class="page-image">
                            <a class="thumbnail" href="/ru/ht/12952/natsionalnaya-khudozhestvennaya-shkola-html-shablon" style="width:260px; height: 586px;" title="Национальная Художественная Школа HTML Шаблон"><img alt="Национальная Художественная Школа HTML Шаблон" class="media-image lazyload noversion" data-src="https://images01.nicepage.com/page/12/95/ru/html-shablon-12952.jpg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=" style="max-width:100%;" /></a>
                            <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#" data-url="https://images01.nicepage.com/page/12/95/ru/html-shablon-full-12952.jpg">Подробнее ↓</a>
                        </div>
                        <div class="page-title one-row ">
                            <a href="/ru/ht/12952/natsionalnaya-khudozhestvennaya-shkola-html-shablon" title="Национальная Художественная Школа HTML Шаблон">Национальная Художественная Школа HTML Шаблон</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>



        <section id="sec-win-download" style="display: none;">
            <div class="container-layout">
                <img alt="" data-image-width="800" data-image-height="400" data-src="//csite.nicepage.com/Images/Site/downloadwinchrome.jpg">
                <p class="u-text">
                    You are downloading Nicepage. Problems?&nbsp;<a href="/get/windows/">Click Here</a>
                </p>
                <p class="u-text">Run Nicepage.exe from the Download Panel</p>
            </div>
        </section>

        <section id="sec-mac-download" style="display: none;">
            <div class="container-layout">
                <img alt="" data-src="//csite.nicepage.com/Images/Site/downloadmacsafari.jpg" >
                <p class="u-text">
                    You are downloading Nicepage. Problems?&nbsp;<a href="/get/mac/">Click Here</a>
                </p>
                <p class="u-text">Run Nicepage.dmg from the Download Panel</p>
            </div>
        </section>
    </section>
    <div id="push"></div>
</div>
<div class="footer">
    <div class="footer-top">
        <div class="container">
            <style>
                .footer-top a {
                    color: #ddd !important;
                    font-size: 16px;
                }
            </style>
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h6>Company</h6>
                    <ul>
                        <li><a href="/doc/frequently-asked-questions-16656">FAQ</a></li>
                        <li><a href="/Terms">Terms of Use</a></li>
                        <li><a href="/Privacy">Privacy Policy</a></li>
                        <li><a href="/Agreement">License Agreement</a></li>
                        <li><a href="/About">About Us</a></li>
                        <li><a href="/partners">Partners</a></li>
                        <li><a href="/Abuse">Abuse</a></li>
                        <li><a href="/Forum/Topic/Create">Contact Support</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h6>Product</h6>
                    <ul>
                        <li><a href="/download">Download</a></li>
                        <li><a href="/features">200+ Features</a></li>
                        <li><a href="/nicepage-review">Review</a></li>
                        <li><a href="/affiliates">Affiliates</a></li>
                        <li><a href="/doc">Documentation</a></li>
                        <li><a href="/forum">Forum</a></li>
                        <li><a href="/blog">Blog</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h6>Themes & Templates</h6>
                    <ul>
                        <li><a href="/website-templates">Website Templates</a></li>
                        <li><a href="/wordpress-themes">WordPress Themes</a></li>
                        <li><a href="/html-templates">HTML Templates</a></li>
                        <li><a href="/css-templates">CSS Templates</a></li>
                        <li><a href="/joomla-templates">Joomla Templates</a></li>
                        <li><a href="/woocommerce-theme">WooCommerce Themes</a></li>
                        <li><a href="/html5-template">HTML5 Templates</a></li>
                        <li><a href="/one-page-template">One Page Templates</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h6>Nicepage</h6>
                    <ul>
                        <li><a href="/website-builder">Website Builder</a></li>
                        <li><a href="/html">HTML Website Builder</a></li>
                        <li><a href="/wordpress">WordPress Website Builder</a></li>
                        <li><a href="/joomla">Joomla Page Builder</a></li>
                        <li><a href="/wysiwyg-html-editor">WYSIWYG HTML Editor</a></li>
                        <li><a href="/static-site-generator">Static Site Generator</a></li>
                        <li><a href="/html-code">HTML Code Generator</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h6>Web Design</h6>
                    <ul>
                        <li><a href="/website-design">Website Designs</a></li>
                        <li><a href="/web-page-design">Web Page Designs</a></li>
                        <li><a href="/web-design">Web Design</a></li>
                        <li><a href="/web-page-designer">Web Page Designer</a></li>
                        <li><a href="/landing-page">Landing Pages</a></li>
                        <li><a href="/homepage-design">Homepage Designs</a></li>
                        <li><a href="/website-mockup">Website  Mockup</a></li>
                    </ul>
                </div>


            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div id="footer-links" class="footer-inline col-sm-12 pull-right">
                    <div style="white-space: nowrap; float: left; text-align: left;">
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="https://facebook.com/nicepageapp" rel="noreferrer" target="_blank" style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#3B5998" cx="56.1" cy="56.1" r="55"></circle>
                                        <path fill="#FFFFFF" d="M73.5,31.6h-9.1c-1.4,0-3.6,0.8-3.6,3.9v8.5h12.6L72,58.3H60.8v40.8H43.9V58.3h-8V43.9h8v-9.2
        	c0-6.7,3.1-17,17-17h12.5v13.9H73.5z"></path>
                                    </svg><span style="position: relative;top: -7px;margin-left: 5px;">Facebook</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://twitter.com/NicepageApp" rel="noreferrer" target="_blank" style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#55ACEE" class="st0" cx="56.1" cy="56.1" r="55"/>
                                        <path fill="#FFFFFF" d="M83.8,47.3c0,0.6,0,1.2,0,1.7c0,17.7-13.5,38.2-38.2,38.2C38,87.2,31,85,25,81.2c1,0.1,2.1,0.2,3.2,0.2
				c6.3,0,12.1-2.1,16.7-5.7c-5.9-0.1-10.8-4-12.5-9.3c0.8,0.2,1.7,0.2,2.5,0.2c1.2,0,2.4-0.2,3.5-0.5c-6.1-1.2-10.8-6.7-10.8-13.1
				c0-0.1,0-0.1,0-0.2c1.8,1,3.9,1.6,6.1,1.7c-3.6-2.4-6-6.5-6-11.2c0-2.5,0.7-4.8,1.8-6.7c6.6,8.1,16.5,13.5,27.6,14
				c-0.2-1-0.3-2-0.3-3.1c0-7.4,6-13.4,13.4-13.4c3.9,0,7.3,1.6,9.8,4.2c3.1-0.6,5.9-1.7,8.5-3.3c-1,3.1-3.1,5.8-5.9,7.4
				c2.7-0.3,5.3-1,7.7-2.1C88.7,43,86.4,45.4,83.8,47.3z"/>
                                    </svg><span style="position: relative;top: -7px;margin-left: 5px;">Twitter</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://youtube.com/nicepage?sub_confirmation=1" rel="noreferrer" target="_blank" style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#D22215" cx="56.1" cy="56.1" r="55"></circle>
                                        <path fill="#FFFFFF" d="M74.9,33.3H37.3c-7.4,0-13.4,6-13.4,13.4v18.8c0,7.4,6,13.4,13.4,13.4h37.6c7.4,0,13.4-6,13.4-13.4V46.7
        	C88.3,39.3,82.3,33.3,74.9,33.3L74.9,33.3z M65.9,57l-17.6,8.4c-0.5,0.2-1-0.1-1-0.6V47.5c0-0.5,0.6-0.9,1-0.6l17.6,8.9
        	C66.4,56,66.4,56.8,65.9,57L65.9,57z"></path>
                                    </svg><span style="position: relative;top: -7px;margin-left: 5px;">YouTube</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://www.pinterest.com/nicepagecom/pins" rel="noreferrer" target="_blank" style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#CB2027" cx="56.1" cy="56.1" r="55"></circle>
                                        <path fill="#FFFFFF" d="M61.1,76.9c-4.7-0.3-6.7-2.7-10.3-5c-2,10.7-4.6,20.9-11.9,26.2c-2.2-16.1,3.3-28.2,5.9-41
        	c-4.4-7.5,0.6-22.5,9.9-18.8c11.6,4.6-10,27.8,4.4,30.7C74.2,72,80.3,42.8,71,33.4C57.5,19.6,31.7,33,34.9,52.6
        	c0.8,4.8,5.8,6.2,2,12.9c-8.7-1.9-11.2-8.8-10.9-17.8C26.5,32.8,39.3,22.5,52.2,21c16.3-1.9,31.6,5.9,33.7,21.2
        	C88.2,59.5,78.6,78.2,61.1,76.9z"></path>
                                    </svg><span style="position: relative;top: -7px;margin-left: 5px;">Pinterest</span></a>
                            </div>
                        </div>
                    </div>

                    <ul class="u-unstyled">
                        <li><a href="/pricing">Premium</a></li>
                        <li>|</li>
                        <li><a href="/affiliates">Affiliates</a></li>
                        <li>|</li>
                        <li><a href="/Forum/Topic/Create?private=1">Contact Support</a></li>
                        <li>|</li>
                        <li><a href="/About">About</a></li>
                        <li>|</li>
                        <li><a href="/Terms">Terms</a></li>
                        <li>|</li>
                        <li><a href="/Privacy">Privacy</a></li>
                        <li>|</li>
                        <li><a href="/Agreement">License</a></li>
                    </ul>
                    <div class="copy">&copy; 2021 Бесплатное программное обеспечение для создания веб-сайтов Nicepage - Все права защищены</div>
                </div>
            </div>
        </div>
    </div>

</div>



<script>
     Intercom scripts
    function loadJSON(path, success, error) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (success) success(JSON.parse(xhr.responseText));
                } else {
                    if (error) error(xhr);
                }
            }
        };
        xhr.open("GET", path, true);
        xhr.send();
    }

    function startChat() {
        var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/vwx04wrq';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}l();}
    }

    loadJSON('/account/getinfo',
         function(data) {
            window.intercomSettings = { app_id: "vwx04wrq", name: data.userInfo.name, email: data.userInfo.email, created_at: data.userInfo.dateCreated };
            startChat();
         },
         function(xhr) {
            window.intercomSettings = { app_id: "vwx04wrq" };
            startChat();
         }
    );

</script>    </body>
</html>
