<!DOCTYPE html>
<html lang="ru">
<head>

    <title>Мультимедийные технологии HTML шаблон</title>
    <link rel="shortcut icon" href="//csite.nicepage.com/favicon.ico" type="image/x-icon">
    <meta charset="utf-8"/>
    <meta name="Keywords" content=" ">
    <meta name="Description"
          content="Мультимедийные технологии. Профессиональный HTML шаблон. Отзывчивый, полностью настраиваемый с помощью простого редактора Drag-n-Drop. Вы можете использовать его для таких предметов, как количество, около, упряжь, мультимедиа, на основании. Категории: Технология.">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://nicepage.com/ru/ht/66149/multimediynye-tekhnologii-html-shablon">
    <meta property="og:title" content="Мультимедийные технологии HTML шаблон">
    <meta property="og:image"
          content="https://images01.nicepage.com/page/66/14/ru/html-shablon-66149.jpg?version=dd4030ac-667d-4f89-bf43-1b40b9734bab">
    <meta property="og:description"
          content="Мультимедийные технологии. Профессиональный HTML шаблон. Отзывчивый, полностью настраиваемый с помощью простого редактора Drag-n-Drop. Вы можете использовать его для таких предметов, как количество, около, упряжь, мультимедиа, на основании. Категории: Технология.">
    <meta property="og:site_name" content="Nicepage.com"/>


    <link rel="preload" href="https://images01.nicepage.com/page/66/14/ru/html-shablon-spreview-66149.webp" as="image"
          type="image/webp" media="(max-width: 450px)">
    <link rel="preload" href="https://images01.nicepage.com/page/66/14/ru/html-shablon-preview-66149.webp" as="image"
          type="image/webp" media="(min-width: 451px)">


    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no,width=device-width">
    <link href="//fonts.googleapis.com/css?family=Roboto|Open+Sans:200,300,400,700,800,900&amp;subset=latin"
          rel="preload" as="font"/>

    <script>
        window.serverSettings = {
            fbAppId: '290410448063109',
            googleAppId: '13150095650-mo8psu2colep6uv90a2mu6r87u87s35a.apps.googleusercontent.com'
        };
        window.isAuthenticated = 0;
        window.logEvent = 1;
        window.userHasAdsParams = 1;
        window.utmSourceFromReferrer = 0;
        window.currentLang = 'ru';
        window.baseUrl = 'html-templates';
        window.currentUrl = '';
        var np_userId = '';
        try {
            np_userId = localStorage.getItem('np_userId');
        } catch (err) {
        }
        if (!np_userId) {
            function S4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }

            // then to call it, plus stitch in '4' in the third group
            np_userId = (S4() + S4() + S4() + S4().substr(0, 3) + S4() + S4() + S4() + S4()).toLowerCase();
            try {
                localStorage.setItem('np_userId', np_userId);
                setCookie('np_userId', np_userId, 365);
            } catch (err) {
            }
        } else if (!readCookie('np_userId')) {
            setCookie('np_userId', np_userId, 365);
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }

        function sendAnalyticsData(eventType, props, cb) {
            var json = {data: {}};
            json.userToken = np_userId;
            json.data.adsParams = readCookie('AdsParameters');
            json.data.ga = readCookie('_ga');
            json.data.gac = readCookie('_gac_UA-88868916-2');
            json.data.userAgent = navigator.userAgent;
            json.data.eventType = eventType;
            json.data.props = props;
            $.ajax({
                'type': 'POST',
                'url': '/Feedback/SendAdsLog',
                'contentType': 'application/json; charset=utf-8',
                'data': JSON.stringify(json),
                'dataType': 'json',
                'complete': cb || function () {
                }
            });
        }

        var isAmplitudeInitialized = false;

        function initializeAmplitudeUser() {
            if (isAmplitudeInitialized) {
                return;
            }
            isAmplitudeInitialized = true;

            identifyAmplitudeUser(null);
        }

        function sendAmplitudeAnalyticsData(eventName, analyticsData, userProperties, callback_function) {
            initializeAmplitudeUser();

            if (userProperties) {
                if (userProperties.utm_source || userProperties.utm_campaign) {
                    var identify = new amplitude.Identify();
                    identify.setOnce("utm_campaign", userProperties.utm_campaign);
                    identify.setOnce("utm_source", userProperties.utm_source);
                    identify.setOnce("utm_content", userProperties.utm_content);
                    identify.setOnce("utm_term", userProperties.utm_term);
                    identify.setOnce("utm_page", userProperties.utm_page);
                    identify.setOnce("utm_page2", userProperties.utm_page);
                    identify.setOnce("referrer", userProperties.referrer);

                    amplitude.getInstance().identify(identify);

                    userProperties.utm_source_last = userProperties.utm_source;
                    userProperties.utm_campaign_last = userProperties.utm_campaign;
                    userProperties.utm_content_last = userProperties.utm_content;
                    userProperties.utm_term_last = userProperties.utm_term;
                    userProperties.utm_page_last = userProperties.utm_page;
                }

                var userProps = _objectWithoutProperties(userProperties, ["utm_campaign", "utm_source", "utm_content", "utm_term", "utm_page", "referrer"]);
                amplitude.getInstance().setUserProperties(userProps);
            }

            analyticsData.WebSite = 'true';
            if (typeof callback_function === 'function') {
                amplitude.getInstance().logEvent(eventName, analyticsData, callback_function);
            } else {
                amplitude.getInstance().logEvent(eventName, analyticsData);
            }
        }

        function identifyAmplitudeUser(userId, token) {
            if (userId) {
                amplitude.getInstance().setUserProperties({
                    "Token": token,
                    "UserId": userId
                });
            }

            var identify = new amplitude.Identify();
            amplitude.getInstance().identify(identify);
            if (userId) {
                amplitude.getInstance().setUserId(userId);
            }
        }

        function _objectWithoutProperties(obj, keys) {
            var target = {};
            for (var i in obj) {
                if (keys.indexOf(i) >= 0) continue;
                if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
                target[i] = obj[i];
            }
            return target;
        }

        function getUtmParams(url) {
            if (!url.searchParams) {
                return {};
            }

            var utmSource = url.searchParams.get("utm_source");
            if (url.searchParams.get("sscid")) {
                utmSource = 'ShareASale';
            } else if (url.searchParams.get("fbclid")) {
                utmSource = 'Facebook';
            }
            var utmCampaign = url.searchParams.get("utm_campaign") || url.searchParams.get("campaign");
            var utmContent = url.searchParams.get("utm_content") || url.searchParams.get("content");
            var utmTerm = url.searchParams.get("utm_term") || url.searchParams.get("term");


            return {utmSource: utmSource, utmCampaign: utmCampaign, utmContent: utmContent, utmTerm: utmTerm}
        }

        function clearPageUrl(pageUrl) {
            if (window.currentLang) {
                pageUrl = pageUrl.replace('/' + window.currentLang, '');
            }
            if (window.currentUrl) {
                pageUrl = pageUrl.replace(window.currentUrl, window.baseUrl);
            }

            var url = pageUrl.replace(/\?.*/, '').replace(/(.)\/$/, '$1');
            var match = url.match(/\/\d+(\/|$)/);
            if (match && match.index > 0) {
                url = url.substring(0, match.index);
            }
            return url.replace(/\/editor\//ig, '/').replace(/\/frame\//ig, '/').toLowerCase();
        }

        function getUtmPageValue(pageUrl) {
            var url = '/';
            var parts = pageUrl.split('/');
            if (pageUrl.startsWith('/') && parts.length > 1) {
                url += parts[1];
            } else {
                return pageUrl;
            }
            return url;
        }

        function sendAnalyticsFromUrl() {
            var hash = window.location.hash;

            var urlIsAvailable = typeof URL === "function" || (navigator.userAgent.indexOf('MSIE') != -1 && typeof URL === 'object');
            if (!urlIsAvailable) {
                return;
            }

            var url = new URL(window.location.href);
            if (hash && hash.indexOf('utm_') >= 0) {
                url = new URL(window.location.origin + window.location.pathname + hash.replace('#', '?'));
            }

            if (!url.searchParams) {
                return;
            }
            var paramsFromCookie = false;
            var utmParams = getUtmParams(url);
            if (userHasAdsParams) {
                var adsParamsFromCookie = readCookie('AdsParameters');
                if (!utmParams.utmCampaign && !utmParams.utmSource && adsParamsFromCookie) {
                    url = new URL(window.location.origin + (adsParamsFromCookie.indexOf('?') === -1 ? '?' : '') + readCookie('AdsParameters'));
                    utmParams = getUtmParams(url);
                    paramsFromCookie = true;
                }
            }

            var source = url.searchParams.get("source");
            var gclid = url.searchParams.get("gclid");

            var needLogEvent = utmParams.utmSource || utmParams.utmCampaign || gclid;
            var fullPageUrl = window.location.pathname.split('?')[0];
            var pageUrl = clearPageUrl(fullPageUrl);

            if (needLogEvent) {
                var eventProps = {
                    "utm_source": utmParams.utmSource || source,
                    "utm_campaign": utmParams.utmCampaign,
                    "utm_content": utmParams.utmContent,
                    "utm_term": utmParams.utmTerm,
                    "utm_page": getUtmPageValue(pageUrl),
                    "page_url": pageUrl,
                    "full_page_url": fullPageUrl
                };
                if (utmParams.utmSource === "elastic") {
                    sendAmplitudeAnalyticsData('Email Click', eventProps);
                }
                if (!paramsFromCookie || utmSourceFromReferrer) {
                    var userProps = {
                        "utm_source": utmParams.utmSource,
                        "utm_campaign": utmParams.utmCampaign,
                        "utm_content": utmParams.utmContent,
                        "utm_term": utmParams.utmTerm,
                        "utm_page": getUtmPageValue(pageUrl),
                        "utm_lang": window.currentLang || '',
                        "referrer": ''
                    };
                    sendAmplitudeAnalyticsData('Campaign', eventProps, userProps);
                }
            }

            if (logEvent && (isAuthenticated || needLogEvent)) {
                var pageEventProps = {
                    'page': pageUrl,
                    'full_page_url': fullPageUrl,
                    'type': 'Template Detail Page',
                    'lang': window.currentLang || ''
                };
                sendAmplitudeAnalyticsData('Page View', pageEventProps);
            }
        }

        document.addEventListener('DOMContentLoaded', function () {
            sendAnalyticsFromUrl();
        });
    </script>
    <!--
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
    h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
    (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
    })(window,document.documentElement,'async-hide','dataLayer',4000,
    {'GTM-KGP3NM3':true});</script>
    -->        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-88868916-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        var options = {};
        // gtag('config', 'GA_TRACKING_ID', { 'optimize_id': 'OPT_CONTAINER_ID'});
        gtag('config', 'UA-88868916-2', options);
        gtag('config', 'AW-797221335');
    </script>
    <!-- Amplitude Code -->
    <script type="text/javascript">
        (function (e, t) {
            var n = e.amplitude || {_q: [], _iq: {}};
            var r = t.createElement("script")
            ;r.type = "text/javascript"
            ;r.integrity = "sha384-d/yhnowERvm+7eCU79T/bYjOiMmq4F11ElWYLmt0ktvYEVgqLDazh4+gW9CKMpYW"
            ;r.crossOrigin = "anonymous";
            r.async = true
            ;r.src = "https://cdn.amplitude.com/libs/amplitude-5.2.2-min.gz.js"
            ;r.onload = function () {
                if (!e.amplitude.runQueuedFunctions) {
                    console.log("[Amplitude] Error: could not load SDK")
                }
            }
            ;var i = t.getElementsByTagName("script")[0];
            i.parentNode.insertBefore(r, i)
            ;

            function s(e, t) {
                e.prototype[t] = function () {
                    this._q.push([t].concat(Array.prototype.slice.call(arguments, 0)));
                    return this
                }
            }

            var o = function () {
                    this._q = [];
                    return this
                }
            ;var a = ["add", "append", "clearAll", "prepend", "set", "setOnce", "unset"]
            ;
            for (var u = 0; u < a.length; u++) {
                s(o, a[u])
            }
            n.Identify = o;
            var c = function () {
                    this._q = []
                    ;
                    return this
                }
            ;var l = ["setProductId", "setQuantity", "setPrice", "setRevenueType", "setEventProperties"]
            ;
            for (var p = 0; p < l.length; p++) {
                s(c, l[p])
            }
            n.Revenue = c
            ;var d = ["init", "logEvent", "logRevenue", "setUserId", "setUserProperties", "setOptOut", "setVersionName", "setDomain", "setDeviceId", "setGlobalUserProperties", "identify", "clearUserProperties", "setGroup", "logRevenueV2", "regenerateDeviceId", "groupIdentify", "onInit", "logEventWithTimestamp", "logEventWithGroups", "setSessionId", "resetSessionId"]
            ;

            function v(e) {
                function t(t) {
                    e[t] = function () {
                        e._q.push([t].concat(Array.prototype.slice.call(arguments, 0)))
                    }
                }

                for (var n = 0; n < d.length; n++) {
                    t(d[n])
                }
            }

            v(n);
            n.getInstance = function (e) {
                e = (!e || e.length === 0 ? "$default_instance" : e).toLowerCase()
                ;
                if (!n._iq.hasOwnProperty(e)) {
                    n._iq[e] = {_q: []};
                    v(n._iq[e])
                }
                return n._iq[e]
            }
            ;e.amplitude = n
        })(window, document);
        amplitude.getInstance().init("878f4709123a5451aff838c1f870b849");
    </script>
    <script>
        var shareasaleSSCID = shareasaleGetParameterByName("sscid");

        function shareasaleSetCookie(e, a, r, s, t) {
            if (e && a) {
                var o, n = s ? "; path=" + s : "", i = t ? "; domain=" + t : "", S = "";
                r && ((o = new Date).setTime(o.getTime() + r), S = "; expires=" + o.toUTCString()), document.cookie = e + "=" + a + S + n + i + "; SameSite=None;Secure"
            }
        }

        function shareasaleGetParameterByName(e, a) {
            a || (a = window.location.href), e = e.replace(/[\[\]]/g, "\\$&");
            var r = new RegExp("[?&]" + e + "(=([^&#]*)|&|#|$)").exec(a);
            return r ? r[2] ? decodeURIComponent(r[2].replace(/\+/g, " ")) : "" : null
        }

        shareasaleSSCID && shareasaleSetCookie("shareasaleSSCID", shareasaleSSCID, 94670778e4, "/");
    </script>
    <script
        src="//csite.nicepage.com/BundledScripts/template-detail-libs.js?version=65711fb9a7fcd1551e0947bf1d5a861061cebb39"
        defer></script>
    <link
        href="//csite.nicepage.com/BundledScripts/template-detail-libs.css?version=65711fb9a7fcd1551e0947bf1d5a861061cebb39"
        rel="stylesheet"/>

    <script>
        function receiveCountryCode() {
            if ($.cookie('UserCountryCode') !== undefined) {
                return;
            }

            $.ajax({
                type: 'HEAD',
                url: 'https://d57e01lyo0mq2.cloudfront.net/location',
                success: function (data, status, jqXHR) {
                    var countryCode = jqXHR.getResponseHeader('cloudfront-viewer-country');
                    if (countryCode) {
                        $.cookie('UserCountryCode', countryCode, {expires: 7, path: '/'});
                    }
                },
                error: function (jqXHR) {
                    var date = new Date();
                    var minutes = 15;
                    date.setTime(date.getTime() + (minutes * 60 * 1000));
                    $.cookie('UserCountryCode', '', {expires: date, path: '/'});
                }
            });
        }

        document.addEventListener('DOMContentLoaded',
            function () {
                receiveCountryCode();
            });
    </script>
    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <link rel="canonical" href="https://nicepage.com/ru/ht/66149/multimediynye-tekhnologii-html-shablon"/>


    <style>
        h1 {
            text-transform: capitalize;
            font-size: 32px;
            margin: 10px 0 20px;
        }

        h2 {
            font-size: 28px;
        }

        .download-btn-container {
            padding-bottom: 10px;
        }

        .preview-row .media-image {
            border: none;
            aspect-ratio: attr(width) / attr(height);
        }

        .preview-row {
            position: relative;
            margin-bottom: 40px;
        }

        .preview-row img,
        .preview-row .hover-like-lightbox {
            display: inline-block;
        }

        .preview-row .hover-like-lightbox {
            overflow: visible;
        }

        .hover-like-lightbox:hover:after {
            display: block;
            content: "";
            background: rgba(0, 0, 0, .2) 100% 100% no-repeat;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .mockup-shadow {
            max-width: 100%;
        }

        .mockup-shadow:before {
            bottom: 0;
            box-shadow: 3px 3px 15px rgba(0, 0, 0, .3);
            display: block;
            content: '';
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            z-index: -1;
        }

        .small-icon {
            vertical-align: middle;
            display: inline-block;
        }

        .u-icon {
            vertical-align: middle;
            height: 40px;
            display: inline-block;
        }

        .icon-g {
            fill: #999;
            width: 48px;
            height: 48px;
            position: relative;
            top: 8px;
            margin-right: 12px;
        }

        .page-description-block {
            margin-top: 30px;
            font-size: 18px;
            line-height: 1.75;
            text-align: left;
        }

        .auth-page-description-block {
            line-height: 1.75;
            font-size: 14px;
        }

        .page-subtitle {
            margin: 20px 0;
            font-size: 18px;
            line-height: 1.75;
        }

        .page-description-block h3 {
            margin-bottom: 20px;
        }

        .win-download,
        .mac-download,
        .edit-online-btn {
            color: #fff;
            padding: 10px 30px;
            margin-bottom: 10px;
            font-size: 20px;
            line-height: 1.4;
            display: inline-block;
        }

        .top-btn {
            margin: 0 0 20px 0;
        }

        @media (min-width: 991px) and (max-width: 1200px) {
            .win-download, .mac-download, .edit-online-btn {
                padding: 10px 26px;
                font-size: 16px;
            }
        }

        @media (max-width: 768px) {
            .win-download, .mac-download, .edit-online-btn {
                margin: 10px 0;
                font-size: 16px;
            }
        }


        .mac-download {
            margin-left: 10px;
            background-color: #4399fa;
            border-color: #4399fa;
        }

        .mac-download:hover, .mac-download:focus {
            color: #fff;
            background-color: #2e8dfa;
            border-color: #2e8dfa;
        }

        .win-download {
            background-color: #f15048;
            border-color: #f15048;
        }

        .win-download:hover, .win-download:focus {
            color: #fff;
            background-color: #ef342b;
            border-color: #ef342b;
        }

        .edit-online-btn {
            margin-left: 10px;
            background-color: #5cb85c;
            border-color: #5cb85c;
        }

        .edit-online-btn.top-btn {
            margin: 0 0 20px 0;
            max-width: 335px;
            width: 100%;
        }

        .edit-online-btn:hover, .edit-online-btn:focus {
            color: #fff;
            background-color: #4bad4b;
            border-color: #4bad4b;
        }

        .feature-icon-label {
            vertical-align: -0.17em;
            margin-left: 10px;
        }

        @media (min-width: 992px) and (max-width: 1280px) {
            #register-input {
                width: 150px;
                font-size: 16px;
                padding: 6px;
            }

            #register-btn {
                font-size: 16px;
                border: none;
                padding: 7px 12px;
                font-weight: 700;
            }

            .feature-icon-label {
                font-size: 14px;
                margin-left: 5px;
            }
        }

        .u-svg-link {
            fill: #f15048;
            height: 40px;
            width: 40px;
            color: #f15048;
            position: relative;
        }

        .u-svg-content {
            width: 0;
            height: 0;
        }

        .right-feature-column {
            padding: 0 10px;
        }

        @media (max-width: 992px) {
            .right-feature-column {
                padding: 0 15px;
            }
        }

        @media (max-width: 768px) {
            .right-feature-column {
                padding-top: 10px;
            }
        }

        .details-keywords {
            margin-left: -10px;
        }

        .details-keywords a {
            display: inline-block;
            padding: 2px 10px;
            margin: 0 0 5px 10px;
            font-size: 13px;
            color: #777;
            border: 1px solid #cecece;
            border-radius: 20px;
            text-transform: lowercase;
            transition: .2s;
        }

        .details-keywords a:hover {
            text-decoration: none;
            color: #337ab7;
            border-color: #337ab7;
        }

        .details-keywords i {
            position: absolute;
            opacity: 0;
        }

        .customize-np-block {
            margin-top: 30px;
            margin-bottom: 20px;
            font-size: 14px;
        }

        .modal .container-layout {
            padding: 20px;
        }

        .modal .u-text {
            font-size: 28px;
            margin: 20px auto 0;
        }
    </style>


    <style>
        .navbar {
            margin-bottom: 0;
        }
    </style>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            if (typeof IOlazy === 'function') {
                var lazy = new IOlazy();
            }
        });
    </script>
</head>
<body>

<div class="wrap wrap-fluid">
    <section id="" class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <style>
                            .breadcrumb {
                                padding: 0;
                                background-color: transparent;
                            }

                            .breadcrumb > .active {
                                color: #666;
                            }
                        </style>


                        <h1>{{$create_landing->title}}</h1>
                        <div style="margin-bottom: 20px; font-size: 14px;">
                            {{$create_landing->title_2}}
                        </div>
                        <div style="margin-bottom: 20px;">
                            <div class="pull-left">
                                ID
                                <text>:</text>
                                66149
                                &nbsp; | &nbsp;
                                Категория :
                                <a href="/ru/c/tekhnologiya-html-shablony">Технология</a></div>
                            <div class="pull-right">
                                <a href="/ru/html-templates/preview/multimediynye-tekhnologii-66149?device=desktop"
                                   target="_blank" class="demo-link" rel="nofollow">
                                    <div class="small-icon">
                                        <svg id="preview" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16"
                                             enable-background="new 0 0 16 16" xml:space="preserve">
                                    <path fill="#808080" d="M12.6,11.8c0.4-0.6,0.7-1.3,0.7-2.2c0-2-1.6-3.7-3.7-3.7S6,7.6,6,9.7s1.6,3.7,3.7,3.7c0.8,0,1.5-0.3,2.2-0.7
                                          l3.4,3.4l0.8-0.8C16,15.2,12.6,11.8,12.6,11.8L12.6,11.8z M9.7,12.2c-1.4,0-2.5-1.1-2.5-2.5s1.1-2.5,2.5-2.5s2.5,1.1,2.5,2.5
                                          S11.1,12.2,9.7,12.2L9.7,12.2z"/>
                                            <polygon fill="none"
                                                     points="8,4 8,1 1,1 1,15 11,15 11,15 3.5,15 3.5,5.5 11,5.5 11,4 "/>
                                            <polygon fill="#808080"
                                                     points="11,15 1,15 1,1 8,1 8,4 11,4 11,5.5 12,5.5 12,3.4 8.3,0 0,0 0,16 12,16 12,14 11,14 "/>
                                </svg>
                                    </div>
                                    <span>Предварительный просмотр демо</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="preview-row">
                            <td><img src="{{ Storage::url($create_landing->image) }}"></td>
                            <div>{!! $create_landing->content !!}</div>
                            <a class="hover-like-lightbox mockup-shadow"
                               href="/ru/html-templates/preview/multimediynye-tekhnologii-66149?device=desktop"
                               rel="nofollow" target="_blank">
                                <picture>
                                    <source
                                        srcset="https://images01.nicepage.com/page/66/14/ru/html-shablon-spreview-66149.webp"
                                        type="image/webp" media="(max-width: 450px)">
                                    <source
                                        srcset="https://images01.nicepage.com/page/66/14/ru/html-shablon-spreview-66149.jpg"
                                        type="image/jpeg" media="(max-width: 450px)">
                                    <source
                                        srcset="https://images01.nicepage.com/page/66/14/ru/html-shablon-preview-66149.webp"
                                        type="image/webp">
                                    <source
                                        srcset="https://images01.nicepage.com/page/66/14/ru/html-shablon-preview-66149.jpg"
                                        type="image/jpeg">
                                    <img alt="Мультимедийные технологии HTML шаблон"
                                         class="media-image img-responsive noversion" height="2820"
                                         src="https://images01.nicepage.com/page/66/14/ru/html-shablon-preview-66149.jpg"
                                         style="" width="750"/>
                                </picture>
                            </a>

                        </div>


                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>Больше от Технология HTML шаблоны</h3>


                    <ul class="thumbnails thumbnails-loading list-unstyled">
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 544px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/17315/novosti-o-tekhnologiyakh-html-shablon"
                                   style="width:260px; height: 494px;" title="Новости О Технологиях"><img
                                        alt="Новости О Технологиях" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/17/31/ru/html-shablon-17315.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/17/31/ru/html-shablon-full-17315.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/17315/novosti-o-tekhnologiyakh-html-shablon"
                                   title="Новости О Технологиях">Новости О Технологиях</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 805px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12853/pikselnye-butony-2-html-shablon"
                                   style="width:260px; height: 755px;" title="Пиксельные Бутоны 2"><img
                                        alt="Пиксельные Бутоны 2" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/85/ru/html-shablon-12853.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/85/ru/html-shablon-full-12853.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12853/pikselnye-butony-2-html-shablon" title="Пиксельные Бутоны 2">Пиксельные
                                    Бутоны 2</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 652px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/283136/resheniya-povyshayut-proizvoditelnost-html-shablon"
                                   style="width:260px; height: 602px;" title="Решения Повышают Производительность"><img
                                        alt="Решения Повышают Производительность" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/28/31/ru/html-shablon-283136.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/28/31/ru/html-shablon-full-283136.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/283136/resheniya-povyshayut-proizvoditelnost-html-shablon"
                                   title="Решения Повышают Производительность">Решения Повышают Производительность</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 565px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12840/luchshaya-kompyuternaya-tekhnika-html-shablon"
                                   style="width:260px; height: 515px;" title="Лучшая Компьютерная Техника"><img
                                        alt="Лучшая Компьютерная Техника" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/84/ru/html-shablon-12840.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/84/ru/html-shablon-full-12840.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12840/luchshaya-kompyuternaya-tekhnika-html-shablon"
                                   title="Лучшая Компьютерная Техника">Лучшая Компьютерная Техника</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 767px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/484142/konsultatsii-po-seo-servisu-html-shablon"
                                   style="width:260px; height: 717px;" title="Консультации По SEO Сервису"><img
                                        alt="Консультации По SEO Сервису" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/48/41/ru/html-shablon-484142.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/48/41/ru/html-shablon-full-484142.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/484142/konsultatsii-po-seo-servisu-html-shablon"
                                   title="Консультации По SEO Сервису">Консультации По SEO Сервису</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 564px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/55597/my-pomogaem-biznesu-povysit-effektivnost-html-shablon"
                                   style="width:260px; height: 514px;"
                                   title="Мы Помогаем Бизнесу Повысить Эффективность"><img
                                        alt="Мы Помогаем Бизнесу Повысить Эффективность"
                                        class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/55/59/ru/html-shablon-55597.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/55/59/ru/html-shablon-full-55597.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/55597/my-pomogaem-biznesu-povysit-effektivnost-html-shablon"
                                   title="Мы Помогаем Бизнесу Повысить Эффективность">Мы Помогаем Бизнесу Повысить
                                    Эффективность</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 673px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/685660/o-virtualnoy-realnosti-html-shablon"
                                   style="width:260px; height: 623px;" title="О Виртуальной Реальности"><img
                                        alt="О Виртуальной Реальности" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/68/56/ru/html-shablon-685660.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/68/56/ru/html-shablon-full-685660.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/685660/o-virtualnoy-realnosti-html-shablon"
                                   title="О Виртуальной Реальности">О Виртуальной Реальности</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 855px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/358254/novosti-kompyuternykh-igr-html-shablon"
                                   style="width:260px; height: 805px;" title="Новости Компьютерных Игр"><img
                                        alt="Новости Компьютерных Игр" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/35/82/ru/html-shablon-358254.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/35/82/ru/html-shablon-full-358254.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/358254/novosti-kompyuternykh-igr-html-shablon"
                                   title="Новости Компьютерных Игр">Новости Компьютерных Игр</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 575px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/17201/peredovye-innovatsionnye-avtomobili-html-shablon"
                                   style="width:260px; height: 525px;" title="Передовые Инновационные Автомобили"><img
                                        alt="Передовые Инновационные Автомобили" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/17/20/ru/html-shablon-17201.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/17/20/ru/html-shablon-full-17201.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/17201/peredovye-innovatsionnye-avtomobili-html-shablon"
                                   title="Передовые Инновационные Автомобили">Передовые Инновационные Автомобили</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 735px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/749340/khostingovaya-kompaniya-html-shablon"
                                   style="width:260px; height: 685px;" title="Хостинговая Компания HTML Шаблон"><img
                                        alt="Хостинговая Компания HTML Шаблон" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/74/93/ru/html-shablon-749340.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/74/93/ru/html-shablon-full-749340.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/749340/khostingovaya-kompaniya-html-shablon"
                                   title="Хостинговая Компания HTML Шаблон">Хостинговая Компания HTML Шаблон</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" style="width: 260px; height: 460px;">
                            <div class="download-block white-block" style="background-color: #ffffff; ">
                                <img class="lazyload"
                                     data-src="//images01.nicepage.com/thumbs/a122014e1e8cdf24af08f98c/de4b91b424635284ad7ea46f/templates-small-animation-color_150.gif"
                                     alt="Самая большая коллекция шаблонов"/>
                                <div style="color: #3c3c3c;" class="download-title">Самая большая коллекция шаблонов
                                </div>
                                <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow"
                                   style="background-color: #ec311e;">Скачать</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 693px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/23397/tekhnologii-budushchego-html-shablon"
                                   style="width:260px; height: 643px;" title="Технологии Будущего"><img
                                        alt="Технологии Будущего" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/23/39/ru/html-shablon-23397.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/23/39/ru/html-shablon-full-23397.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/23397/tekhnologii-budushchego-html-shablon" title="Технологии Будущего">Технологии
                                    Будущего</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 504px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/19816/vse-tekhnologii-html-shablon"
                                   style="width:260px; height: 454px;" title="Все Технологии"><img alt="Все Технологии"
                                                                                                   class="media-image lazyload noversion"
                                                                                                   data-src="https://images01.nicepage.com/page/19/81/ru/html-shablon-19816.jpg"
                                                                                                   src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                                                                                   style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/19/81/ru/html-shablon-full-19816.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/19816/vse-tekhnologii-html-shablon" title="Все Технологии">Все
                                    Технологии</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 552px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/13068/umnaya-butylka-html-shablon"
                                   style="width:260px; height: 502px;" title="Умная Бутылка"><img alt="Умная Бутылка"
                                                                                                  class="media-image lazyload noversion"
                                                                                                  data-src="https://images01.nicepage.com/page/13/06/ru/html-shablon-13068.jpg"
                                                                                                  src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                                                                                  style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/13/06/ru/html-shablon-full-13068.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/13068/umnaya-butylka-html-shablon" title="Умная Бутылка">Умная
                                    Бутылка</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 578px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/31086/uzhe-tekhnologii-budushchego-html-shablon"
                                   style="width:260px; height: 528px;" title="Уже Технологии Будущего"><img
                                        alt="Уже Технологии Будущего" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/31/08/ru/html-shablon-31086.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/31/08/ru/html-shablon-full-31086.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/31086/uzhe-tekhnologii-budushchego-html-shablon"
                                   title="Уже Технологии Будущего">Уже Технологии Будущего</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 533px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/23167/kosmos-html-shablon"
                                   style="width:260px; height: 483px;" title="Космос"><img alt="Космос"
                                                                                           class="media-image lazyload noversion"
                                                                                           data-src="https://images01.nicepage.com/page/23/16/ru/html-shablon-23167.jpg"
                                                                                           src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                                                                           style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/23/16/ru/html-shablon-full-23167.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/23167/kosmos-html-shablon" title="Космос">Космос</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 486px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12521/puleneprobivaemye-prilozheniya-html-shablon"
                                   style="width:260px; height: 436px;" title="Пуленепробиваемые Приложения"><img
                                        alt="Пуленепробиваемые Приложения" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/52/ru/html-shablon-12521.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/52/ru/html-shablon-full-12521.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12521/puleneprobivaemye-prilozheniya-html-shablon"
                                   title="Пуленепробиваемые Приложения">Пуленепробиваемые Приложения</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 591px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/24145/luchshie-naushniki-html-shablon"
                                   style="width:260px; height: 541px;" title="Лучшие Наушники"><img
                                        alt="Лучшие Наушники" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/24/14/ru/html-shablon-24145.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/24/14/ru/html-shablon-full-24145.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/24145/luchshie-naushniki-html-shablon" title="Лучшие Наушники">Лучшие
                                    Наушники</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 651px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/34941/tekhno-navyki-v-biznese-html-shablon"
                                   style="width:260px; height: 601px;" title="Техно Навыки В Бизнесе"><img
                                        alt="Техно Навыки В Бизнесе" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/34/94/ru/html-shablon-34941.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/34/94/ru/html-shablon-full-34941.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/34941/tekhno-navyki-v-biznese-html-shablon"
                                   title="Техно Навыки В Бизнесе">Техно Навыки В Бизнесе</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 599px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/17226/lider-v-tekhnologiyakh-html-shablon"
                                   style="width:260px; height: 549px;" title="Лидер В Технологиях"><img
                                        alt="Лидер В Технологиях" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/17/22/ru/html-shablon-17226.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/17/22/ru/html-shablon-full-17226.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/17226/lider-v-tekhnologiyakh-html-shablon" title="Лидер В Технологиях">Лидер
                                    В Технологиях</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 747px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12863/video-tekhnologii-html-shablon"
                                   style="width:260px; height: 697px;" title="Видео Технологии HTML Шаблон"><img
                                        alt="Видео Технологии HTML Шаблон" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/86/ru/html-shablon-12863.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/86/ru/html-shablon-full-12863.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12863/video-tekhnologii-html-shablon"
                                   title="Видео Технологии HTML Шаблон">Видео Технологии HTML Шаблон</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" style="width: 260px; height: 420px;">
                            <div class="download-block " style="background-color: #363636; ">
                                <img class="lazyload"
                                     data-src="//images01.nicepage.com/thumbs/a122014e1e8cdf24af08f98c/8739790df40451e683e5a152/templates-small-animation-dark_150.gif"
                                     alt="000+ шаблонов веб-сайтов"/>
                                <div style="color: #ffffff;" class="download-title">000+ шаблонов веб-сайтов</div>
                                <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow"
                                   style="background-color: #cd30c0;">Скачать</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 625px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/313607/resheniya-po-bezopasnosti-tsod-html-shablon"
                                   style="width:260px; height: 575px;" title="Решения По Безопасности ЦОД"><img
                                        alt="Решения По Безопасности ЦОД" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/31/36/ru/html-shablon-313607.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/31/36/ru/html-shablon-full-313607.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/313607/resheniya-po-bezopasnosti-tsod-html-shablon"
                                   title="Решения По Безопасности ЦОД">Решения По Безопасности ЦОД</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 715px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/488180/1000-klientov-ispolzuyushchikh-nashe-prilozhenie-html-shablon"
                                   style="width:260px; height: 665px;"
                                   title="1000+ Клиентов, Использующих Наше Приложение"><img
                                        alt="1000+ Клиентов, Использующих Наше Приложение"
                                        class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/48/81/ru/html-shablon-488180.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/48/81/ru/html-shablon-full-488180.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/488180/1000-klientov-ispolzuyushchikh-nashe-prilozhenie-html-shablon"
                                   title="1000+ Клиентов, Использующих Наше Приложение">1000+ Клиентов, Использующих
                                    Наше Приложение</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 592px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/17189/prevoskhodnoe-kachestvo-muzyki-html-shablon"
                                   style="width:260px; height: 542px;" title="Превосходное Качество Музыки"><img
                                        alt="Превосходное Качество Музыки" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/17/18/ru/html-shablon-17189.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/17/18/ru/html-shablon-full-17189.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/17189/prevoskhodnoe-kachestvo-muzyki-html-shablon"
                                   title="Превосходное Качество Музыки">Превосходное Качество Музыки</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 541px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/71369/tekhnika-remonta-doma-html-shablon"
                                   style="width:260px; height: 491px;" title="Техника Ремонта Дома"><img
                                        alt="Техника Ремонта Дома" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/71/36/ru/html-shablon-71369.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/71/36/ru/html-shablon-full-71369.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/71369/tekhnika-remonta-doma-html-shablon" title="Техника Ремонта Дома">Техника
                                    Ремонта Дома</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 462px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/19116/sovremennye-veb-tekhnologii-html-shablon"
                                   style="width:260px; height: 412px;" title="Современные Веб-Технологии"><img
                                        alt="Современные Веб-Технологии" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/19/11/ru/html-shablon-19116.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/19/11/ru/html-shablon-full-19116.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/19116/sovremennye-veb-tekhnologii-html-shablon"
                                   title="Современные Веб-Технологии">Современные Веб-Технологии</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 811px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/284799/kachestvennaya-razrabotka-prilozheniy-html-shablon"
                                   style="width:260px; height: 761px;" title="Качественная Разработка Приложений"><img
                                        alt="Качественная Разработка Приложений" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/28/47/ru/html-shablon-284799.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/28/47/ru/html-shablon-full-284799.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/284799/kachestvennaya-razrabotka-prilozheniy-html-shablon"
                                   title="Качественная Разработка Приложений">Качественная Разработка Приложений</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 703px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/23330/progress-v-robototekhnike-html-shablon"
                                   style="width:260px; height: 653px;" title="Прогресс В Робототехнике"><img
                                        alt="Прогресс В Робототехнике" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/23/33/ru/html-shablon-23330.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/23/33/ru/html-shablon-full-23330.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/23330/progress-v-robototekhnike-html-shablon"
                                   title="Прогресс В Робототехнике">Прогресс В Робототехнике</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 619px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/476756/razrabotchik-programmnogo-obespecheniya-html-shablon"
                                   style="width:260px; height: 569px;" title="Разработчик Программного Обеспечения"><img
                                        alt="Разработчик Программного Обеспечения"
                                        class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/47/67/ru/html-shablon-476756.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/47/67/ru/html-shablon-full-476756.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/476756/razrabotchik-programmnogo-obespecheniya-html-shablon"
                                   title="Разработчик Программного Обеспечения">Разработчик Программного Обеспечения</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 743px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/20573/tsifrovoy-mir-tekhnologiy-html-shablon"
                                   style="width:260px; height: 693px;" title="Цифровой Мир Технологий"><img
                                        alt="Цифровой Мир Технологий" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/20/57/ru/html-shablon-20573.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/20/57/ru/html-shablon-full-20573.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/20573/tsifrovoy-mir-tekhnologiy-html-shablon"
                                   title="Цифровой Мир Технологий">Цифровой Мир Технологий</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 629px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/24285/beats-wireless-html-shablon"
                                   style="width:260px; height: 579px;" title="Beats Wireless HTML Шаблон"><img
                                        alt="Beats Wireless HTML Шаблон" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/24/28/ru/html-shablon-24285.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/24/28/ru/html-shablon-full-24285.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/24285/beats-wireless-html-shablon" title="Beats Wireless HTML Шаблон">Beats
                                    Wireless HTML Шаблон</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" style="width: 260px; height: 413px;">
                            <div class="download-block white-block" style="background-color: #ffffff; font-size: 24px;">
                                <img class="lazyload"
                                     data-src="//images01.nicepage.com/thumbs/a122014e1e8cdf24af08f98c/b09223d099135909bcb0d471/drag-n-drop2_150.gif"
                                     alt="Простой редактор перетаскивания"/>
                                <div style="color: #3c3c3c;" class="download-title">Простой редактор перетаскивания
                                </div>
                                <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow"
                                   style="background-color: #ec311e;">Скачать</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 798px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/373318/razrabotka-mobilnykh-prilozheniy-html-shablon"
                                   style="width:260px; height: 748px;" title="Разработка Мобильных Приложений"><img
                                        alt="Разработка Мобильных Приложений" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/37/33/ru/html-shablon-373318.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/37/33/ru/html-shablon-full-373318.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/373318/razrabotka-mobilnykh-prilozheniy-html-shablon"
                                   title="Разработка Мобильных Приложений">Разработка Мобильных Приложений</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 698px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/66149/multimediynye-tekhnologii-html-shablon"
                                   style="width:260px; height: 648px;" title="Мультимедийные Технологии"><img
                                        alt="Мультимедийные Технологии" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/66/14/ru/html-shablon-66149.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/66/14/ru/html-shablon-full-66149.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/66149/multimediynye-tekhnologii-html-shablon"
                                   title="Мультимедийные Технологии">Мультимедийные Технологии</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 739px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/14292/pervyy-chelovek-na-marse-html-shablon"
                                   style="width:260px; height: 689px;" title="Первый Человек На Марсе"><img
                                        alt="Первый Человек На Марсе" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/14/29/ru/html-shablon-14292.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/14/29/ru/html-shablon-full-14292.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/14292/pervyy-chelovek-na-marse-html-shablon"
                                   title="Первый Человек На Марсе">Первый Человек На Марсе</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 636px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12789/puskovye-ustanovki-snapchat-html-shablon"
                                   style="width:260px; height: 586px;" title="Пусковые Установки Snapchat"><img
                                        alt="Пусковые Установки Snapchat" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/78/ru/html-shablon-12789.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/78/ru/html-shablon-full-12789.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12789/puskovye-ustanovki-snapchat-html-shablon"
                                   title="Пусковые Установки Snapchat">Пусковые Установки Snapchat</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 760px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12295/dizayn-tekhnologiy-budushchego-html-shablon"
                                   style="width:260px; height: 710px;" title="Дизайн Технологий Будущего"><img
                                        alt="Дизайн Технологий Будущего" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/29/ru/html-shablon-12295.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/29/ru/html-shablon-full-12295.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12295/dizayn-tekhnologiy-budushchego-html-shablon"
                                   title="Дизайн Технологий Будущего">Дизайн Технологий Будущего</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 817px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/522290/developerskaya-kompaniya-html-shablon"
                                   style="width:260px; height: 767px;" title="Девелоперская Компания"><img
                                        alt="Девелоперская Компания" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/52/22/ru/html-shablon-522290.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/52/22/ru/html-shablon-full-522290.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/522290/developerskaya-kompaniya-html-shablon"
                                   title="Девелоперская Компания">Девелоперская Компания</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 550px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/18333/flex-2-html-shablon"
                                   style="width:260px; height: 500px;" title="Flex 2"><img alt="Flex 2"
                                                                                           class="media-image lazyload noversion"
                                                                                           data-src="https://images01.nicepage.com/page/18/33/ru/html-shablon-18333.jpg"
                                                                                           src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                                                                           style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/18/33/ru/html-shablon-full-18333.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/18333/flex-2-html-shablon" title="Flex 2">Flex 2</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 573px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/18337/budushchie-tekhnologii-html-shablon"
                                   style="width:260px; height: 523px;" title="Будущие Технологии"><img
                                        alt="Будущие Технологии" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/18/33/ru/html-shablon-18337.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/18/33/ru/html-shablon-full-18337.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/18337/budushchie-tekhnologii-html-shablon" title="Будущие Технологии">Будущие
                                    Технологии</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 564px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/22491/tekhnologiya-dostavki-html-shablon"
                                   style="width:260px; height: 514px;" title="Технология Доставки"><img
                                        alt="Технология Доставки" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/22/49/ru/html-shablon-22491.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/22/49/ru/html-shablon-full-22491.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/22491/tekhnologiya-dostavki-html-shablon" title="Технология Доставки">Технология
                                    Доставки</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 963px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/462226/bystroe-razvitie-html-shablon"
                                   style="width:260px; height: 913px;" title="Быстрое Развитие HTML Шаблон"><img
                                        alt="Быстрое Развитие HTML Шаблон" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/46/22/ru/html-shablon-462226.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/46/22/ru/html-shablon-full-462226.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/462226/bystroe-razvitie-html-shablon"
                                   title="Быстрое Развитие HTML Шаблон">Быстрое Развитие HTML Шаблон</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" style="width: 260px; height: 320px;">
                            <div class="download-block " style="background-color: #363636; ">
                                <div style="color: #ffffff;" class="download-title">Создайте свой собственный сайт</div>
                                <a class="btn btn-lg download-btn" href="/download" type="button" rel="nofollow"
                                   style="background-color: #cd30c0;">Скачать</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 506px;">
                            <div class="page-image">
                                <a class="thumbnail" href="/ru/ht/12619/elegantnye-resheniya-v-biznese-html-shablon"
                                   style="width:260px; height: 456px;" title="Элегантные Решения В Бизнесе"><img
                                        alt="Элегантные Решения В Бизнесе" class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/12/61/ru/html-shablon-12619.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/12/61/ru/html-shablon-full-12619.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/12619/elegantnye-resheniya-v-biznese-html-shablon"
                                   title="Элегантные Решения В Бизнесе">Элегантные Решения В Бизнесе</a>
                            </div>
                        </li>
                        <li class="thumbnail-item" data-height="0" style="width: 260px; height: 787px;">
                            <div class="page-image">
                                <a class="thumbnail"
                                   href="/ru/ht/897701/kompaniya-po-razrabotke-programmnogo-obespecheniya-polnogo-tsikla-html-shablon"
                                   style="width:260px; height: 737px;"
                                   title="Компания По Разработке Программного Обеспечения Полного Цикла"><img
                                        alt="Компания По Разработке Программного Обеспечения Полного Цикла"
                                        class="media-image lazyload noversion"
                                        data-src="https://images01.nicepage.com/page/89/77/ru/html-shablon-897701.jpg"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs="
                                        style="max-width:100%;"/></a>
                                <a class="btn btn-default btn-sm expand-page-button pull-right page-more" href="#"
                                   data-url="https://images01.nicepage.com/page/89/77/ru/html-shablon-full-897701.jpg">Подробнее
                                    ↓</a>
                            </div>
                            <div class="page-title one-row ">
                                <a href="/ru/ht/897701/kompaniya-po-razrabotke-programmnogo-obespecheniya-polnogo-tsikla-html-shablon"
                                   title="Компания По Разработке Программного Обеспечения Полного Цикла">Компания По
                                    Разработке Программного Обеспечения Полного Цикла</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>


            <section id="sec-win-download" style="display: none;">
                <div class="container-layout">
                    <img alt="" data-image-width="800" data-image-height="400"
                         data-src="//csite.nicepage.com/Images/Site/downloadwinchrome.jpg">
                    <p class="u-text">
                        You are downloading Nicepage. Problems?&nbsp;<a href="/get/windows/">Click Here</a>
                    </p>
                    <p class="u-text">Run Nicepage.exe from the Download Panel</p>
                </div>
            </section>

            <section id="sec-mac-download" style="display: none;">
                <div class="container-layout">
                    <img alt="" data-src="//csite.nicepage.com/Images/Site/downloadmacsafari.jpg">
                    <p class="u-text">
                        You are downloading Nicepage. Problems?&nbsp;<a href="/get/mac/">Click Here</a>
                    </p>
                    <p class="u-text">Run Nicepage.dmg from the Download Panel</p>
                </div>
            </section>
    </section>
    <div id="push"></div>
</div>
<div class="footer">
    <div class="footer-top">
        <div class="container">
            <style>
                .footer-top a {
                    color: #ddd !important;
                    font-size: 16px;
                }
            </style>
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h6>Company</h6>
                    <ul>
                        <li><a href="/doc/frequently-asked-questions-16656">FAQ</a></li>
                        <li><a href="/Terms">Terms of Use</a></li>
                        <li><a href="/Privacy">Privacy Policy</a></li>
                        <li><a href="/Agreement">License Agreement</a></li>
                        <li><a href="/About">About Us</a></li>
                        <li><a href="/partners">Partners</a></li>
                        <li><a href="/Abuse">Abuse</a></li>
                        <li><a href="/Forum/Topic/Create">Contact Support</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h6>Product</h6>
                    <ul>
                        <li><a href="/download">Download</a></li>
                        <li><a href="/features">200+ Features</a></li>
                        <li><a href="/nicepage-review">Review</a></li>
                        <li><a href="/affiliates">Affiliates</a></li>
                        <li><a href="/doc">Documentation</a></li>
                        <li><a href="/forum">Forum</a></li>
                        <li><a href="/blog">Blog</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h6>Themes & Templates</h6>
                    <ul>
                        <li><a href="/website-templates">Website Templates</a></li>
                        <li><a href="/wordpress-themes">WordPress Themes</a></li>
                        <li><a href="/html-templates">HTML Templates</a></li>
                        <li><a href="/css-templates">CSS Templates</a></li>
                        <li><a href="/joomla-templates">Joomla Templates</a></li>
                        <li><a href="/woocommerce-theme">WooCommerce Themes</a></li>
                        <li><a href="/html5-template">HTML5 Templates</a></li>
                        <li><a href="/one-page-template">One Page Templates</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <h6>Nicepage</h6>
                    <ul>
                        <li><a href="/website-builder">Website Builder</a></li>
                        <li><a href="/html">HTML Website Builder</a></li>
                        <li><a href="/wordpress">WordPress Website Builder</a></li>
                        <li><a href="/joomla">Joomla Page Builder</a></li>
                        <li><a href="/wysiwyg-html-editor">WYSIWYG HTML Editor</a></li>
                        <li><a href="/static-site-generator">Static Site Generator</a></li>
                        <li><a href="/html-code">HTML Code Generator</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <h6>Web Design</h6>
                    <ul>
                        <li><a href="/website-design">Website Designs</a></li>
                        <li><a href="/web-page-design">Web Page Designs</a></li>
                        <li><a href="/web-design">Web Design</a></li>
                        <li><a href="/web-page-designer">Web Page Designer</a></li>
                        <li><a href="/landing-page">Landing Pages</a></li>
                        <li><a href="/homepage-design">Homepage Designs</a></li>
                        <li><a href="/website-mockup">Website Mockup</a></li>
                    </ul>
                </div>


            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div id="footer-links" class="footer-inline col-sm-12 pull-right">
                    <div style="white-space: nowrap; float: left; text-align: left;">
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="https://facebook.com/nicepageapp" rel="noreferrer" target="_blank"
                                   style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#3B5998" cx="56.1" cy="56.1" r="55"></circle>
                                        <path fill="#FFFFFF" d="M73.5,31.6h-9.1c-1.4,0-3.6,0.8-3.6,3.9v8.5h12.6L72,58.3H60.8v40.8H43.9V58.3h-8V43.9h8v-9.2
        	c0-6.7,3.1-17,17-17h12.5v13.9H73.5z"></path>
                                    </svg>
                                    <span style="position: relative;top: -7px;margin-left: 5px;">Facebook</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://twitter.com/NicepageApp" rel="noreferrer" target="_blank"
                                   style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#55ACEE" class="st0" cx="56.1" cy="56.1" r="55"/>
                                        <path fill="#FFFFFF" d="M83.8,47.3c0,0.6,0,1.2,0,1.7c0,17.7-13.5,38.2-38.2,38.2C38,87.2,31,85,25,81.2c1,0.1,2.1,0.2,3.2,0.2
				c6.3,0,12.1-2.1,16.7-5.7c-5.9-0.1-10.8-4-12.5-9.3c0.8,0.2,1.7,0.2,2.5,0.2c1.2,0,2.4-0.2,3.5-0.5c-6.1-1.2-10.8-6.7-10.8-13.1
				c0-0.1,0-0.1,0-0.2c1.8,1,3.9,1.6,6.1,1.7c-3.6-2.4-6-6.5-6-11.2c0-2.5,0.7-4.8,1.8-6.7c6.6,8.1,16.5,13.5,27.6,14
				c-0.2-1-0.3-2-0.3-3.1c0-7.4,6-13.4,13.4-13.4c3.9,0,7.3,1.6,9.8,4.2c3.1-0.6,5.9-1.7,8.5-3.3c-1,3.1-3.1,5.8-5.9,7.4
				c2.7-0.3,5.3-1,7.7-2.1C88.7,43,86.4,45.4,83.8,47.3z"/>
                                    </svg>
                                    <span style="position: relative;top: -7px;margin-left: 5px;">Twitter</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://youtube.com/nicepage?sub_confirmation=1" rel="noreferrer"
                                   target="_blank" style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#D22215" cx="56.1" cy="56.1" r="55"></circle>
                                        <path fill="#FFFFFF" d="M74.9,33.3H37.3c-7.4,0-13.4,6-13.4,13.4v18.8c0,7.4,6,13.4,13.4,13.4h37.6c7.4,0,13.4-6,13.4-13.4V46.7
        	C88.3,39.3,82.3,33.3,74.9,33.3L74.9,33.3z M65.9,57l-17.6,8.4c-0.5,0.2-1-0.1-1-0.6V47.5c0-0.5,0.6-0.9,1-0.6l17.6,8.9
        	C66.4,56,66.4,56.8,65.9,57L65.9,57z"></path>
                                    </svg>
                                    <span style="position: relative;top: -7px;margin-left: 5px;">YouTube</span></a>
                            </div>
                            <div class="col-sm-3">
                                <a href="https://www.pinterest.com/nicepagecom/pins" rel="noreferrer" target="_blank"
                                   style="margin-right: 10px;">
                                    <svg x="0px" y="0px" viewBox="0 0 112.2 112.2" style="width: 24px;">
                                        <circle fill="#CB2027" cx="56.1" cy="56.1" r="55"></circle>
                                        <path fill="#FFFFFF" d="M61.1,76.9c-4.7-0.3-6.7-2.7-10.3-5c-2,10.7-4.6,20.9-11.9,26.2c-2.2-16.1,3.3-28.2,5.9-41
        	c-4.4-7.5,0.6-22.5,9.9-18.8c11.6,4.6-10,27.8,4.4,30.7C74.2,72,80.3,42.8,71,33.4C57.5,19.6,31.7,33,34.9,52.6
        	c0.8,4.8,5.8,6.2,2,12.9c-8.7-1.9-11.2-8.8-10.9-17.8C26.5,32.8,39.3,22.5,52.2,21c16.3-1.9,31.6,5.9,33.7,21.2
        	C88.2,59.5,78.6,78.2,61.1,76.9z"></path>
                                    </svg>
                                    <span style="position: relative;top: -7px;margin-left: 5px;">Pinterest</span></a>
                            </div>
                        </div>
                    </div>
                    <!--
                    <ul class="u-unstyled">
                        <li><a href="/pricing">Premium</a></li>
                        <li>|</li>
                        <li><a href="/affiliates">Affiliates</a></li>
                        <li>|</li>
                        <li><a href="/Forum/Topic/Create?private=1">Contact Support</a></li>
                        <li>|</li>
                        <li><a href="/About">About</a></li>
                        <li>|</li>
                        <li><a href="/Terms">Terms</a></li>
                        <li>|</li>
                        <li><a href="/Privacy">Privacy</a></li>
                        <li>|</li>
                        <li><a href="/Agreement">License</a></li>
                    </ul>
                    -->
                    <div class="copy">&copy; 2021 Бесплатное программное обеспечение для создания веб-сайтов Nicepage -
                        Все права защищены
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script>
    /* Intercom scripts
    function loadJSON(path, success, error) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (success) success(JSON.parse(xhr.responseText));
                } else {
                    if (error) error(xhr);
                }
            }
        };
        xhr.open("GET", path, true);
        xhr.send();
    }

    function startChat() {
        var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/vwx04wrq';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}l();}
    }

    loadJSON('/account/getinfo',
         function(data) {
            window.intercomSettings = { app_id: "vwx04wrq", name: data.userInfo.name, email: data.userInfo.email, created_at: data.userInfo.dateCreated };
            startChat();
         },
         function(xhr) {
            window.intercomSettings = { app_id: "vwx04wrq" };
            startChat();
         }
    );
    */
</script>
</body>
</html>
