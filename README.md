<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

We present You the foundation for your online application, in which the admin panel is already configured.
<br>Already implemented:
<br> - authentication
<br> - CRUD of users
<br> - CRUD of Landing

Getting Started:

1. "Place the parent directory on a web server or local server"

2. Run
   ###### composer install
3. Copy the "env.example" file in the root directory and rename it to ".env"
4. Create database
5. In .env file update the database configuration variables
6. Generate APP_KEY - php artisan key: generate
7. Run migration and seed
   ######php artisan migrate --seed
8. Register subdomain (test1, test2) in the local server
